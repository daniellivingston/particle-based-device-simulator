CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      Writes electrons number at each mesh point
C
C      Important note: This is device independent routine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine write_electron_distribution()

       include 'max_electron_number.h'
       include 'max_grid_size.h'

       common
     &/mesh/mesh_sizex,mesh_sizey
     &/mesh_size/nx_max,ny_max
     &/variables/p(max_electron_number,8),ip(max_electron_number),
     1           energy(max_electron_number)
     &/electrons_used/n_used

       real mesh_sizex,mesh_sizey
       integer elec_number(0:nx_size,0:ny_size)
       integer i,j,n,n_used

       do i = 0,nx_max
       do j = 0,ny_max
          elec_number(i,j) = 0
       enddo
       enddo

       do n = 1,n_used
          if(ip(n).ne.9)then
             i = int(p(n,5)/mesh_sizex+0.5)
             j = int(p(n,6)/mesh_sizey+0.5)
             if(i.gt.nx_max)i = nx_max
             if(i.lt.0)then
                print*,'found negative coordinate along x'
                print*,'x-position = ',x_position
                i = 0
             endif
             if(j.gt.ny_max)j = ny_max
             if(j.lt.0)j = 0
             if(j.lt.0)then
                print*,'found negative coordinate along y'
                print*,'y-position = ',y_position
                j = 0
             endif
             elec_number(i,j) = elec_number(i,j) + 1
          endif
       enddo

       open(unit=1,file='electron_distribution',
     1             status='unknown')
       do i = 0,nx_max
       do j = 0,ny_max
          write(1,*)elec_number(i,j)
       enddo
       enddo
       close(1)

       return
       end
