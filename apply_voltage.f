CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      APPLY VOLTAGE AT THE SOURCE, DRAIN AND BACK CONTACT
C
C      Important note: This is MOSFET specific subroutine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine apply_voltage()
       
       include 'max_grid_size.h'
       include 'scattering_table_parameters.h'

       common
     &/mesh_size/nx_max,ny_max
     &/source_drain/n_s,n_d
     &/intrinsic_density/intrinsic_carrier_density
     &/oxide_thickness/oxide_thickness
     &/delta_Ec/delta_Ec
     &/dielec_func/eps_high,eps_low,
     1         eps_sc,eps_oxide,gamma
     &/grid/xx(-1:nx_size),yy(-1:ny_size)
     &/potential/fai(0:nx_size,0:ny_size)
     &/bias/beta(0:nx_size)
     &/input_voltages/V_source,V_drain,V_gate,V_substrate
     &/coulomb/doping_density(n_doping_regions),
     1         Energy_debye(n_doping_regions)

      integer nx_max,ny_max,n,i,j
      real intrinsic_carrier_density
      real dop_term

C     Source region
       j = 0
       do i = 0,n_s
          n = int(find_region(i,j))
          dop_term = doping_density(n)/intrinsic_carrier_density
          if(dop_term.gt.0)then
             factor = dop_term + sqrt(dop_term**2.+1.)
             fai(i,j) = log(dop_term)
          elseif(dop_term.le.0)then
             dop_term = - dop_term
             factor = dop_term + sqrt(dop_term**2.+1.)
             fai(i,j) = - log(dop_term)
          endif
          fai(i,j) = fai(i,j) + V_source
       enddo

C     Drain region
       j = 0
       do i = n_d,nx_max
          n = int(find_region(i,j))
          dop_term = doping_density(n)/intrinsic_carrier_density
          if(dop_term.gt.0)then
             factor = dop_term + sqrt(dop_term**2.+1.)
             fai(i,j)=log(dop_term)
          elseif(dop_term.le.0)then
             dop_term=-dop_term
             factor = dop_term + sqrt(dop_term**2.+1.)
             fai(i,j)=-log(dop_term)
          endif
          fai(i,j) = fai(i,j) + V_drain
       enddo

C     Substrate
       j = ny_max
       do i = 0,nx_max
          n = int(find_region(i,j))
          dop_term = doping_density(n)/intrinsic_carrier_density
          if(dop_term.gt.0)then
             factor = dop_term + sqrt(dop_term**2.+1.)
             fai(i,j) = log(dop_term)
          elseif(dop_term.le.0)then
             dop_term = - dop_term
             factor = dop_term + sqrt(dop_term**2.+1.)
             fai(i,j) = - log(dop_term)
          endif
          fai(i,j) = fai(i,j) + V_substrate
       enddo

C     Gate contact
       do i = n_s+1,n_d-1
          beta(i) = gamma*(xx(i)+xx(i-1))/oxide_thickness
     1            * (delta_Ec+V_gate)
       enddo

       return
       end
