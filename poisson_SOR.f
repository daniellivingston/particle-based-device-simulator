CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      CALCULATE DISCRETIZATION COEFFICIENTS FOR POISSON EQUATION:
C
C            => flag_dom = 1 (ohmic contact)
C            => flag_dom = 2 (semiconductor/oxide interface)
C            => flag_dom = 3 (bulk region)
C
C      Important note: This is device independent routine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine coefficients_poisson()

       include 'max_grid_size.h'

       common
     &/mesh_size/nx_max,ny_max
     &/oxide_thickness/oxide_thickness
     &/dielec_func/eps_high,eps_low,
     1             eps_sc,eps_oxide,gamma
     &/grid/xx(-1:nx_size),yy(-1:ny_size)
     &/flag_domain/flag_dom(0:nx_size,0:ny_size)
     &/coeff_poisson_1/a_coeff(0:nx_size,0:ny_size)
     &/coeff_poisson_2/b_coeff(0:nx_size,0:ny_size)
     &/coeff_poisson_3/c_coeff(0:nx_size,0:ny_size)
     &/coeff_poisson_4/d_coeff(0:nx_size,0:ny_size)
     &/coeff_poisson_5/e_coeff(0:nx_size,0:ny_size)
     &/coeff_poisson_6/alpha(0:nx_size,0:ny_size)

       integer nx_max,ny_max

       do i = 0,nx_max
       do j = 0,ny_max
           if(flag_dom(i,j).eq.3.)then
              a_coeff(i,j) = (yy(j)+yy(j-1))/xx(i)
              b_coeff(i,j) = (yy(j)+yy(j-1))/xx(i-1)
              if(i.eq.nx_max)then
                 a_coeff(i,j) = 0
                 b_coeff(i,j) = 2.*b_coeff(i,j)
              elseif(i.eq.0)then
                 a_coeff(i,j) = 2.*a_coeff(i,j)
                 b_coeff(i,j) = 0.
              endif
              d_coeff(i,j) = (xx(i)+xx(i-1))/yy(j)
              e_coeff(i,j) = (xx(i)+xx(i-1))/yy(j-1)
              if(j.eq.ny_max)then
                 d_coeff(i,j) = 0.
                 e_coeff(i,j) = 2.*e_coeff(i,j)
              elseif(j.eq.0)then
                 e_coeff(i,j) = 0.
                 d_coeff(i,j) = 2.*d_coeff(i,j)
              endif
              c_coeff(i,j) = - (a_coeff(i,j) + b_coeff(i,j)
     1                       +  d_coeff(i,j) + e_coeff(i,j))
              alpha(i,j) = 0.5*(xx(i)+xx(i-1))*(yy(j)+yy(j-1))
           elseif(flag_dom(i,j).eq.1.) then
               a_coeff(i,j) = 0.
               b_coeff(i,j) = 0.
               d_coeff(i,j) = 0.
               e_coeff(i,j) = 0.
               c_coeff(i,j) = 1.
           elseif(flag_dom(i,j).eq.2.) then
               a_coeff(i,j) = yy(j)/xx(i)
               b_coeff(i,j) = yy(j)/xx(i-1)
               d_coeff(i,j) = (xx(i)+xx(i-1))/yy(j)
               e_coeff(i,j) = 0.
               c_coeff(i,j) = -(a_coeff(i,j) + b_coeff(i,j)
     1               + d_coeff(i,j)*(1.+gamma*yy(j)/oxide_thickness))
               alpha(i,j)=0.5*yy(j)*(xx(i)+xx(i-1))
           endif
      enddo
      enddo

      return
      end


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      SOLVE 2D-POISSON EQUATION USING THE SOR-METHOD:
C
C            => flag_solution = 0 (equilibrium case)
C            => flag_solution = 1 (non-equilibrium case)
C
C      Important note: This is device independent routine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine poisson_SOR(flag_solution)

       include 'max_grid_size.h'

       common
     &/mesh_size/nx_max,ny_max
     &/conv_parameters/omega,tolerance
     &/flag_domain/flag_dom(0:nx_size,0:ny_size)
     &/coeff_poisson_1/a_coeff(0:nx_size,0:ny_size)
     &/coeff_poisson_2/b_coeff(0:nx_size,0:ny_size)
     &/coeff_poisson_3/c_coeff(0:nx_size,0:ny_size)
     &/coeff_poisson_4/d_coeff(0:nx_size,0:ny_size)
     &/coeff_poisson_5/e_coeff(0:nx_size,0:ny_size)
     &/coeff_poisson_6/alpha(0:nx_size,0:ny_size)
     &/potential/fai(0:nx_size,0:ny_size)
     &/doping/doping(0:nx_size,0:ny_size)
     &/electron_density/electron_density(0:nx_size,0:ny_size)
     &/bias/beta(0:nx_size)

       integer nx_max,ny_max
       real forcing_func(0:nx_size,0:ny_size)
       real c2(0:nx_size,0:ny_size)
       real newerror
       logical flag_conv
       real flag_solution

       flag_conv = .false.

       do i = 0,nx_max
       do j = 0,ny_max
          if(flag_dom(i,j).eq.1)then
             forcing_func(i,j) = fai(i,j)
             c2(i,j) = c_coeff(i,j)
          endif
       enddo
       enddo

       do while(.not.flag_conv)

C     Calculate the forcing function

       do i = 0,nx_max
       do j = 0,ny_max
          if(flag_dom(i,j).eq.1)goto 11
          if(flag_solution.eq.0.)then
             denn = exp(fai(i,j))
             denn_inv = 1./denn
          elseif(flag_solution.eq.1)then
             denn = electron_density(i,j)
             denn_inv = exp(-fai(i,j))
          endif
          denn_sum = denn + denn_inv
          if(flag_dom(i,j).eq.3) then
             c2(i,j) = c_coeff(i,j)-alpha(i,j)*denn_sum
             temp_term = denn_inv-denn+doping(i,j)
     1                 + fai(i,j)*denn_sum
             forcing_func(i,j) = -alpha(i,j)*temp_term
             goto 11
          elseif(flag_dom(i,j).eq.2) then
             c2(i,j) = c_coeff(i,j)-alpha(i,j)*denn_sum
             temp_term = denn_inv-denn+doping(i,j)
     1                 + fai(i,j)*denn_sum
             forcing_func(i,j)= -alpha(i,j)*temp_term-beta(i)
          endif
11        continue
       enddo
       enddo

C     Update potential

       error = 0.

       do i = 0,nx_max
       do j = 0,ny_max
          x1 = (forcing_func(i,j) - a_coeff(i,j)*fai(i+1,j)
     1       - b_coeff(i,j)*fai(i-1,j) - d_coeff(i,j)*fai(i,j+1)
     2       - e_coeff(i,j)*fai(i,j-1))/c2(i,j)
          x2 = fai(i,j) + omega*(x1-fai(i,j))
          newerror = abs(x2-fai(i,j))
          if(newerror.gt.error)error=newerror
          fai(i,j) = x2
       enddo
       enddo

       if(error.lt.tolerance)flag_conv = .true.

       enddo

       return
       end


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      SUBROUTINE THAT WRITES THE MESH ALONG X-AXIS AND Y-AXIS
C
C      Important note: This is device independent routine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine write_mesh()

       include 'max_grid_size.h'

       common
     &/debye/debye_length
     &/mesh_size/nx_max,ny_max
     &/grid/xx(-1:nx_size),yy(-1:ny_size)

C     Write x-axis
       open(unit=1,file='x_axis',status='unknown')
       x_position = 0.
       do i = 0,nx_max
          write(1,*)x_position
          x_position = x_position + xx(i)*debye_length
       enddo
       close(1)

C     Write y-axis
       open(unit=1,file='y_axis',status='unknown')
       y_position = 0.
       do j = 0,ny_max
          write(1,*)y_position
          y_position = y_position + yy(j)*debye_length
       enddo
       close(1)

       return
       end


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      WRITE DEVICE RELATED PARAMETERS:
C
C            => Electrostatic potential (real units)
C            => Electron density (real units)
C            => Hole density (real units)
C            => Total charge density (real units)
C
C      Important note: This is device independent routine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine write_potential(flag_solution)
       include 'max_grid_size.h'

       common
     &/temp/tem,Vt
     &/mesh_size/nx_max,ny_max
     &/delta_Ec/delta_Ec
     &/debye/debye_length
     &/intrinsic_density/intrinsic_carrier_density
     &/potential/fai(0:nx_size,0:ny_size)
     &/potential2/fai2(0:nx_size,0:ny_size)
     &/doping/doping(0:nx_size,0:ny_size)
     &/electron_density/electron_density(0:nx_size,0:ny_size)

      real intrinsic_carrier_density
      real flag_solution

      open(unit=1,file='potential',status='unknown')
      open(unit=2,file='electron_density',status='unknown')
      open(unit=3,file='hole_density',status='unknown')
      open(unit=4,file='charge_density',status='unknown')
      open(unit=5,file='doping',status='unknown')

      do i=0,nx_max
      do j=0,ny_max

         if(flag_solution.eq.0.)then
            denn=exp(fai2(i,j))
         elseif(flag_solution.eq.1.)then
            denn=electron_density(i,j)
         endif
         denn_inv = exp(-fai2(i,j))
         ro = denn_inv-denn+doping(i,j)
         write(1,*)(delta_Ec - fai2(i,j))*Vt
         write(2,*)denn*intrinsic_carrier_density
         write(3,*)denn_inv*intrinsic_carrier_density
         write(4,*)ro*intrinsic_carrier_density
         write(5,*)doping(i,j)*intrinsic_carrier_density

      enddo
      enddo

      close(1)
      close(2)
      close(3)
      close(4)
      close(5)

      return
      end
