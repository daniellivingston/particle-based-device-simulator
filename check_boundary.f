CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     Check boundaries including:
C            => Artificial boundaries
C            => Source and drain regions
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine check_boundary()

       common
     &/mesh/mesh_sizex,mesh_sizey
     &/length/device_length
     &/depth/device_depth
     &/width/device_width
     &/mesh_size/nx_max,ny_max
     &/source_drain/n_s,n_d
     &/particle_atr/kx,ky,kz,iv,e,i_region,
     1              x_position,y_position,ix_fix,jy_fix
     &/drain_current/idd_out,idd_eli,idd_cre
     &/source_current/iss_out,iss_eli,iss_cre

      real kx,ky,kz,e,mesh_sizex,mesh_sizey
      integer iv,i_region,ix_fix,jy_fix
      integer ix,jy,n_s,n_d
     
C     Check side and bottom boundary

       if(x_position.lt.0.)then
          x_position=-x_position
          kx=-kx
       elseif(x_position.gt.device_length)then
          x_position=device_length-(x_position-device_length)
          kx=-kx
       elseif(y_position.gt.device_depth)then
c         print*,'reached bottom boundary'
         y_position=device_depth-(y_position-device_depth)
         ky=-ky
c        iv = 9
       endif

C     Check top boundary
      
       if(y_position.le.0) then
          ix = int(x_position/mesh_sizex+0.5)
          if(ix.gt.nx_max)ix=nx_max
          if(ix.lt.0)ix=0
          if(ix.le.n_s)then
             iv = 9
             iss_out = iss_out + 1
             goto 11
          elseif(ix.ge.n_d)then
             iv = 9
             idd_out = idd_out + 1
             goto 11
          elseif(ix.gt.n_s.and.ix.lt.n_d)then
             y_position=-y_position
             ky=-ky
          endif
       endif
11     continue
 
       return
       end
