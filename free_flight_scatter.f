CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C       PERFORM THE FREE-FLIGHT AND SCATTER 
C       PART WITHIN ONE TIME INTERVAL    
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine free_flight_scatter()

       include 'scattering_table_parameters.h'
       include 'max_electron_number.h'
        
       common
     &/ran_var/iso
     &/mesh/mesh_sizex,mesh_sizey
     &/inv_mesh/inv_mesh_sizex,inv_mesh_sizey
     &/mesh_size/nx_max,ny_max
     &/time_1/dt,dtau,tot_time
     &/electrons_used/n_used
     &/scatt_par/emax,de,w(n_scatt_max,n_doping_regions),
     1           tau_max(n_doping_regions),
     2           max_scatt_mech(n_doping_regions)
     &/variables/p(max_electron_number,8),ip(max_electron_number),
     1           energy(max_electron_number)
     &/particle_atr/kx,ky,kz,iv,e,i_region,
     1              x_position,y_position,ix_fix,jy_fix
     &/drain_current/idd_out,idd_eli,idd_cre
     &/source_current/iss_out,iss_eli,iss_cre
        
      integer i_region,iv,ix_fix,jy_fix
      real kx,ky,kz,e,x_position,y_position
      real mesh_sizex,inv_mesh_sizex
      real mesh_sizey,inv_mesh_sizey

      inv_mesh_sizex = 1./mesh_sizex
      inv_mesh_sizey = 1./mesh_sizey

c     print*,'Enter the free-flight portion'
c     print*,'Number of particles used = ',n_used

C     Reset the electron number

      idd_out = 0
      idd_eli = 0
      idd_cre = 0
      iss_out = 0
      iss_eli = 0
      iss_cre = 0   
c     call count_used_particles(n_electrons)
c     print*,'Number of electrons in use = ',n_electrons

      do i = 1, n_used	! loop for all used carriers

C     Inverse mapping of particle atributes

      kx = p(i,1)
      ky = p(i,2)
      kz = p(i,3)
      dtau = p(i,4)
      x_position = p(i,5)
      y_position = p(i,6)
C     z_position = p(i,7)
      i_region = int(p(i,8))
      iv = ip(i)
      e = energy(i)
      ix_fix = int(x_position/mesh_sizex)
      jy_fix = int(y_position/mesh_sizey)
      if(ix_fix.lt.0)ix_fix = 0
      if(ix_fix.gt.nx_max)ix_fix = nx_max
      if(jy_fix.lt.0)jy_fix = 0
      if(jy_fix.gt.ny_max)jy_fix = ny_max
      if(iv.eq.9)goto 403  

C     Initial free-flight of the carriers

      dte = dtau
      if(dte.ge.dt)then
         dt2=dt
      else
         dt2=dte
      endif
      call drift(dt2)
      if(iv.eq.9)goto 403    
      if(dte.gt.dt)goto 401

C     Free-flight and scatter part

402   dte2 = dte
      call scatter_carrier()          	
219   rr=ran(iso)
      if(rr.le.1e-6) go to 219
      dt3=-(log(rr))*tau_max(iv)
      dtp = dt - dte2	! remaining time to scatter in dt-interval
      if(dt3.le.dtp)then
	 dt2 = dt3
      else
	 dt2 = dtp
      endif 
      call drift(dt2)
      if(iv.eq.9)goto 403
	   
c     Update times

      dte2 = dte2 + dt3
      dte = dte2
      if(dte.lt.dt)goto 402
           
401   dte = dte - dt
      dtau = dte

403   continue

C     Map particle atributes

      p(i,1) = kx
      p(i,2) = ky
      p(i,3) = kz
      p(i,4) = dtau
      p(i,5) = x_position
      p(i,6) = y_position
c     p(i,7) = z_position
      p(i,8) = i_region
      ip(i) = iv
      energy(i) = e	             

      enddo

c      call count_used_particles(n_electrons)
c      print*,'Number of electrons in use = ',n_electrons
c      print*,'End of the free-flight portion'
c      print*,'  '

      return	
      end
