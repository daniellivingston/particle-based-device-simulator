clear all;
figure
x = ones(76);
y = ones(201);
load x_axis;
load y_axis;
x(:,1)=x_axis(:,1)
y(:,1)=y_axis(:,1);
z=ones(201,76);
load potential;
z(:)=potential(:,1);
surf(z)

%figure
%z2=ones(201,76);
%load electron_density;
%z2(:)=electron_density(:,1);
%surf(z2)

figure
z3=ones(201,76);
load doping;
z3(:)=doping(:,1);
surf(z3)

figure
z3=ones(201,76);
load electron_distribution;
z3(:)=electron_distribution(:,1);
surf(z3)