CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      Subroutine that checks the charge neutrality of the source and
C      drain ohmic contacts
C
C      Important note: This is MOSFET specific subroutine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine check_source_drain_contacts()
      
      include 'max_electron_number.h'
      include 'max_grid_size.h'

      common
     &/mesh/mesh_sizex,mesh_sizey
     &/inv_mesh/inv_mesh_sizex,inv_mesh_sizey
     &/mesh_size/nx_max,ny_max
     &/source_drain/n_s,n_d
     &/electrons_used/n_used
     &/source_drain_carriers/
     1        nsource_drain_carriers(0:nx_size)
     &/variables/p(max_electron_number,8),ip(max_electron_number),
     1           energy(max_electron_number)
     &/drain_current/idd_out,idd_eli,idd_cre
     &/source_current/iss_out,iss_eli,iss_cre
  
      real mesh_sizex,mesh_sizey
      real inv_mesh_sizex,inv_mesh_sizey
      integer npt(0:nx_size)
      integer nx_max,ny_max,ix,jy
      integer n_number
      
C    Delete extra carriers at the source and drain contacts

c      print*,'Enter the delete portion at the contacts'
c      print*,'Number of particles used = ',n_used

      do i = 0, nx_max
         npt(i) = 0
      enddo

      do n = 1,n_used
      if(ip(n).ne.9)then
         x_position = p(n,5)
         y_position = p(n,6)
C        z_position = p(n,7)
         ix = int(x_position/mesh_sizex+0.5)
         jy = int(y_position/mesh_sizey+0.5)
         if(ix.gt.nx_max)ix=nx_max
         n_number = nsource_drain_carriers(ix)
         if(jy.eq.0.and.
     1      (ix.le.n_s.or.ix.ge.n_d))then
            if(npt(ix).lt.n_number)then
               npt(ix) = npt(ix) + 1
            else
               ip(n) = 9
               if(ix.ge.n_d)idd_eli = idd_eli + 1
               if(ix.le.n_s)iss_eli = iss_eli + 1
            endif
         endif
      endif
      enddo
      
c      print*,'idd_eli = ',idd_eli
c      print*,'iss_eli = ',iss_eli
c      call count_used_particles(ne)
c      print*,'Number of electrons in use = ',ne
c      print*,'End of the delete portion of the code'
c      print*,'  '      

C     Create carriers at the source and drain contacts

c      print*,'Enter the create portion at the contacts'
c      print*,'Number of particles used = ',n_used

      jy = 0
      do ix = 0,nx_max
      if(ix.le.n_s.or.ix.ge.n_d)then
         nele_diff = nsource_drain_carriers(ix)-npt(ix)
         if(nele_diff.le.0)goto 11
         if(nele_diff.gt.0)then
            do nn = 1,nele_diff
               ne = n_used + nn
               call init_kspace(ne,ix,jy)
               call init_realspace(ne,ix,jy)
            enddo
            n_used = n_used + nele_diff
            if(ix.ge.n_d)idd_cre = idd_cre + nele_diff
            if(ix.le.n_s)iss_cre = iss_cre + nele_diff
         endif
11       continue
      endif
      enddo

c      print*,'idd_cre = ',idd_cre
c      print*,'iss_cre = ',iss_cre
c      call count_used_particles(ne)
c      print*,'Number of electrons in use = ',ne
c      print*,'Number of particles used = ',n_used
c      print*,'End of the create portion of the code'
c      print*,'  '
      
      return
      end



CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      Calculate the number of carriers in the source and drain regions
C      based on doping
C
C      Important note: This is MOSFET specific subroutine 
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine source_drain_carrier_number()
       
       include 'max_grid_size.h'

       common
     &/ran_var/iso
     &/mesh/mesh_sizex,mesh_sizey
     &/mesh_size/nx_max,ny_max
     &/doping/doping(0:nx_size,0:ny_size)
     &/intrinsic_density/intrinsic_carrier_density
     &/width/device_width
     &/source_drain_carriers/
     1        nsource_drain_carriers(0:nx_size)

      real intrinsic_carrier_density,denn
      real mesh_sizex,mesh_sizey
      integer nx_max,ny_max,n_number

      cell_volume = 0.5*mesh_sizex*mesh_sizey*device_width

      j = 0
      do i = 0,nx_max
         denn = doping(i,j)
     1        * intrinsic_carrier_density
         if(i.eq.0.or.i.eq.nx_max)denn=0.5*denn
         factor = denn*cell_volume
	 n_number = int(factor)
c         error = factor - float(n_number)
c         if(ran(iso).le.error)then
c            n_number = n_number + 1
c         endif
         nsource_drain_carriers(i) = n_number                   
      enddo

      return
      end
