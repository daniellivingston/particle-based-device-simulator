CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      Delete extra particles based on their ip-number
C      (if iv = 9, that means that the particle is not used)
C
C      Important note: This is device independent routine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine delete_particles()
      
      include 'max_electron_number.h'

      common
     &/variables/p(max_electron_number,8),ip(max_electron_number),
     1           energy(max_electron_number)
     &/electrons_used/n_used

      integer nx
      logical flag_conv
      
c      print*,'Enter the delete subroutine'
c      print*,'Number of particles used = ',n_used
c      call count_used_particles(ne)
c      print*,'Number of electrons in use = ',ne    

      do i = 1,n_used
         
         if(ip(i).eq.9)then
            flag_conv = .false.
            n_fix = n_used + 1
            do while(.not.flag_conv)
               if(ip(n_fix).ne.9)then
                  ip(i) = ip(n_fix)
                  do j = 1,8
                     p(i,j)=p(n_fix,j)
                  enddo
                  energy(i)=energy(n_fix)
                  ip(n_fix )= 9
                  n_fix = n_fix - 1
                  flag_conv = .true.
               endif
               n_fix = n_fix - 1
               if(n_fix.lt.i)flag_conv = .true.              
            enddo
         endif
         
      enddo
      
14    if(ip(n_used).eq.9)then
         n_used = n_used - 1
         goto 14
      endif
      
c      call count_used_particles(ne)
c      print*,'Number of particles used = ',n_used
c      print*,'Number of electrons in use = ',ne
c      print*,'End of the delete subroutine'
c      print*,'  '
            
      return
      end
