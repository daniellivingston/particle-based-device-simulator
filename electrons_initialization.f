CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      Calculate the initial number of electrons based on the
C      equilibrium solution of the Poisson equation
C
C      Important note:
C      -  At this point this subroutine assumes uniform mesh
C
C      Important note: This is device independent routine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine electrons_initialization()

       include 'max_electron_number.h'
       include 'max_grid_size.h'

       common
     &/mesh_size/nx_max,ny_max
     &/intrinsic_density/intrinsic_carrier_density
     &/debye/debye_length
     &/mesh/mesh_sizex,mesh_sizey
     &/width/device_width
     &/source_drain/n_s,n_d
     &/potential/fai(0:nx_size,0:ny_size)
     &/variables/p(max_electron_number,8),ip(max_electron_number),
     1           energy(max_electron_number)
     &/electrons_used/n_used

       real intrinsic_carrier_density
       real mesh_sizex,mesh_sizey
       integer nx_max,ny_max,np_ij

       cell_volume = mesh_sizex*mesh_sizey*device_width

       n_electron = 0
       do i = 0,nx_max
       do j = 0,ny_max
          denn = exp(fai(i,j))*intrinsic_carrier_density
          if(i.eq.0.or.i.eq.nx_max)denn = denn/2.
          if(j.eq.0.or.j.eq.ny_max)denn = denn/2.
	  np_ij = int(denn*cell_volume)
	  if (np_ij.eq.0)goto 20
	  do m = 1,np_ij
	     ne = ne + 1
             if(ne.gt.max_electron_number)then
                print*,'Actual number of electrons = ',ne
                print*,'No of electrons exceeds ',max_electron_number
                print*,'  '
                stop
	       endif
	     call init_kspace(ne,i,j)
	     call init_realspace(ne,i,j)
	 enddo
20     continue
       enddo
       enddo

       n_used = ne
       print*,'Maximum # of electrons allowed = ',max_electron_number
       print*,'Number of electrons initialized =',n_used
       print*,'  '

       do i = n_used+1,max_electron_number
          ip(i) = 9
       enddo

       return
       end
