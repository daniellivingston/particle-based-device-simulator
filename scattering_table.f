CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C       SUBROUTINE THAT CREATES THE SCATTERING TABLE
C       flag_mech = 1 ==> isotropic scattering process
C       flag_mech = 2 ==> polar optical phonons
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine sc_table()
       include 'scattering_table_parameters.h'

       common
     &/pi/pi,two_pi 
     &/temp/tem,Vt
     &/fund_const/q,h,kb,am0,eps_0
     &/mass/am(3),smh(3),hhm(3)
     &/nonp/af(3),af2(3),af4(3)
     &/valley_splitting/split_L_gamma,split_X_gamma
     &/equiv_valleys/eq_valleys_gamma,eq_valleys_L,eq_valleys_X
     &/scatt_par/emax,de,w(n_scatt_max,n_doping_regions),
     1           tau_max(n_doping_regions),
     2           max_scatt_mech(n_doping_regions)
     &/scatt_par2/flag_mech(n_scatt_max,n_doping_regions),
     1           i_valley(10,3)
     &/table/scatt_table(n_lev,n_scatt_max,n_doping_regions)
     &/dielec_func/eps_sc,eps_oxide,gamma,
     1           eps_high,eps_low
     &/density/density,sound_velocity
     &/select_acouctic/acoustic_gamma,acoustic_L,acoustic_X
     &/select_Coulomb/Coulomb_scattering
     &/select_polar/polar_gamma,polar_L,polar_X
     &/select_intervalley_1/intervalley_gamma_L
     &/select_intervalley_2/intervalley_gamma_X
     &/select_intervalley_3/intervalley_L_gamma
     &/select_intervalley_4/intervalley_L_L
     &/select_intervalley_5/intervalley_L_X
     &/select_intervalley_6/intervalley_X_gamma
     &/select_intervalley_7/intervalley_X_L
     &/select_intervalley_8/intervalley_X_X
     &/sigma_acoustic/sigma_gamma,sigma_L,sigma_X
     &/polar_en/polar_en_gamma,polar_en_L,polar_en_X
     &/doping_regions/n_region
     &/coulomb/doping_density(n_doping_regions),
     1         Energy_debye(n_doping_regions)
     &/Def_pot_1/DefPot_gamma_L,DefPot_gamma_X
     &/Def_pot_2/DefPot_L_gamma,DefPot_L_L,DefPot_L_X
     &/Def_pot_3/DefPot_X_gamma,DefPot_X_L,DefPot_X_X
     &/interval_phonons_1/phonon_gamma_L,phonon_gamma_X
     &/interval_phonons_2/phonon_L_gamma,phonon_L_L,phonon_L_X
     &/interval_phonons_3/phonon_X_gamma,phonon_X_L,phonon_X_X
     &/acoustic/sigma
     &/intervalley1/coupling_constant
     &/intervalley2/delta_fi,final_valleys,i_final 

      integer n_lev, n_region
      character*30 out_file_1, out_file_2
      real kb

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C     CREATE TABLE FOR THE GAMMA VALLEY
C     Scattering mechanism:  - acoustic phonons
C                            - polar optical phonons
C                            - gamma-to-L intervalley    
C                            - gamma-to-X intervalley
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      iv = 1
      i_count = 0

C    Acoustic phonons scattering rate

      if(acoustic_gamma.eq.1)then
         sigma = sigma_gamma
         out_file_1 = 'acoustic_gamma'
         call acoustic_rate(i_count,iv,out_file_1)
      endif

C    Coulomb scattering rate - Brooks-Herring approach

      if(Coulomb_scattering.eq.1)then
         out_file_1 = 'Coulomb_gamma'
         call Coulomb_BH(i_count,iv,out_file_1)
      endif      
      
C     Polar optical phonons scattering rate

      if(polar_gamma.eq.1)then
         w0 = polar_en_gamma
         out_file_1 = 'polar_gamma_ab'
         out_file_2 = 'polar_gamma_em'
         call polar_rate(i_count,iv,w0,
     1                   out_file_1,out_file_2)
      endif

C    Intervalley scattering: gamma to L valley

      if(intervalley_gamma_L.eq.1)then
         w0 = phonon_gamma_L 
         coupling_constant = DefPot_gamma_L
         delta_fi = split_L_gamma
         final_valleys = eq_valleys_L
         i_final = 2   ! final_valley number
         out_file_1 = 'intervalley_gamma_L_ab'
         out_file_2 = 'intervalley_gamma_L_em'     
         call intervalley(i_count,iv,w0,
     1                    out_file_1,out_file_2)
      endif

C    Intervalley scattering: gamma to X valley

      if(intervalley_gamma_X.eq.1)then
         w0 = phonon_gamma_X
         coupling_constant = DefPot_gamma_X
         delta_fi = split_X_gamma
         final_valleys = eq_valleys_X
         i_final = 3   ! final_valley number
         out_file_1 = 'intervalley_gamma_X_ab'
         out_file_2 = 'intervalley_gamma_X_em'     
         call intervalley(i_count,iv,w0,
     1                    out_file_1,out_file_2)
      endif    
                       
      call renormalize_table(iv,i_count)
      print*,'Mechanisms in the gamma valley = ',i_count
      print*,'  '
      
      if(i_count.gt.0)then
         open(unit=31,file='gamma_table_renormalized',
     1                status='unknown')
         do i = 1,n_lev
            ee = float(i)*de
            write(31,32)ee,(scatt_table(i,k,1),k=1,10)
32          format(2X,11(F8.3,2X))
         enddo          
      endif
      close(31)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C     CREATE TABLE FOR THE L VALLEY
C     Scattering mechanism:  - acoustic phonons
C                            - polar optical phonons
C                            - L-to-gamma intervalley    
C                            - L-to-L intervalley
C                            - L-to-X intervalley
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      iv = 2
      i_count = 0
      
C     Acoustic phonons scattering rate

      if(acoustic_L.eq.1)then
         sigma = sigma_L
         out_file_1 = 'acoustic_L'
         call acoustic_rate(i_count,iv,out_file_1)
      endif

C    Coulomb scattering rate - Brooks-Herring approach

      if(Coulomb_scattering.eq.1)then
         out_file_1 = 'Coulomb_L'
         call Coulomb_BH(i_count,iv,out_file_1)
      endif
      
C     Polar optical phonons scattering rate

      if(polar_L.eq.1)then
         w0 = polar_en_L
         out_file_1 = 'polar_L_ab'
         out_file_2 = 'polar_L_em'
         call polar_rate(i_count,iv,w0,
     1                   out_file_1,out_file_2)
      endif

C    Intervalley scattering: L to gamma valley

      if(intervalley_L_gamma.eq.1)then
         w0 = phonon_L_gamma
         coupling_constant = DefPot_L_gamma
         delta_fi = - split_L_gamma
         final_valleys = eq_valleys_gamma
         i_final = 1   ! final_valley
         out_file_1 = 'intervalley_L_gamma_ab'
         out_file_2 = 'intervalley_L_gamma_em'     
         call intervalley(i_count,iv,w0,
     1                    out_file_1,out_file_2)
      endif

C    Intervalley scattering: L to L valley

      if(intervalley_L_L.eq.1)then
         w0 = phonon_L_L
         coupling_constant = DefPot_L_L
         delta_fi = 0.
         final_valleys = eq_valleys_L - 1.
         i_final = 2   ! final_valley
         out_file_1 = 'intervalley_L_L_ab'
         out_file_2 = 'intervalley_L_L_em'     
         call intervalley(i_count,iv,w0,
     1                    out_file_1,out_file_2)
      endif

C    Intervalley scattering: L to X valley

      if(intervalley_L_X.eq.1)then
         w0 = phonon_L_X
         coupling_constant = DefPot_L_X 
         delta_fi = split_X_gamma - split_L_gamma
         final_valleys = eq_valleys_X
         i_final = 3   ! final_valley
         out_file_1 = 'intervalley_L_X_ab'
         out_file_2 = 'intervalley_L_X_em'     
         call intervalley(i_count,iv,w0,
     1                out_file_1,out_file_2)
      endif    
                       
      call renormalize_table(iv,i_count)
      print*,'Mechanisms in the L valley = ',i_count
      print*,'  '
      
      if(i_count.gt.0)then
         open(unit=31,file='L_table_renormalized',
     1                status='unknown')
         do i = 1,n_lev
            ee = float(i)*de
            write(31,33)ee,(scatt_table(i,k,2),k=1,10)
33          format(2X,11(F8.3,2X))
         enddo          
      endif
      close(31)

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C     CREATE TABLE FOR THE X VALLEY
C     Scattering mechanism:  - acoustic phonons
C                            - polar optical phonons
C                            - X-to-gamma intervalley    
C                            - X-to-L intervalley
C                            - X-to-X intervalley
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      iv = 3
      i_count = 0
      
C     Acoustic phonons scattering rate

      if(acoustic_X.eq.1)then
         sigma = sigma_X
         out_file_1 = 'acoustic_X'
         call acoustic_rate(i_count,iv,out_file_1)
      endif

C    Coulomb scattering rate - Brooks-Herring approach

      if(Coulomb_scattering.eq.1)then
         out_file_1 = 'Coulomb_X'
         call Coulomb_BH(i_count,iv,out_file_1)
      endif
      
C     Polar optical phonons scattering rate

      if(polar_X.eq.1)then
         w0 = polar_en_X
         out_file_1 = 'polar_X_ab'
         out_file_2 = 'polar_X_em'
         call polar_rate(i_count,iv,w0,
     1                   out_file_1,out_file_2)
      endif

C    Intervalley scattering: X to gamma valley

      if(intervalley_X_gamma.eq.1)then
         w0 = phonon_X_gamma
         coupling_constant = DefPot_X_gamma
         delta_fi = - split_X_gamma
         final_valleys = eq_valleys_gamma
         i_final = 1   ! final_valley
         out_file_1 = 'intervalley_X_gamma_ab'
         out_file_2 = 'intervalley_X_gamma_em'     
         call intervalley(i_count,iv,w0,
     1                    out_file_1,out_file_2)
      endif

C    Intervalley scattering: X to L valley

      if(intervalley_X_L.eq.1)then
         w0 = phonon_X_L 
         coupling_constant = DefPot_X_L
         delta_fi = split_L_gamma - split_X_gamma
         final_valleys = eq_valleys_L
         i_final = 2   ! final_valley
         print*,w0,coupling_const
         out_file_1 = 'intervalley_X_L_ab'
         out_file_2 = 'intervalley_X_L_em'     
         call intervalley(i_count,iv,w0,
     1                    out_file_1,out_file_2)
      endif

C    Intervalley scattering: X to X valley

      if(intervalley_X_X.eq.1)then
         w0 = phonon_X_X
         coupling_constant = DefPot_X_X
         delta_fi = 0.
         final_valleys = eq_valleys_X - 1.
         i_final = 3   ! final_valley
         out_file_1 = 'intervalley_X_X_ab'
         out_file_2 = 'intervalley_X_X_em'     
         call intervalley(i_count,iv,w0,
     1                    out_file_1,out_file_2)
      endif    
                       
      call renormalize_table(iv,i_count)
      print*,'Mechanisms in the X valley = ',i_count
      print*,'  '
      
      if(i_count.gt.0)then
         open(unit=31,file='X_table_renormalized',
     1                status='unknown')
         do i = 1,n_lev
            ee = float(i)*de
            write(31,34)ee,(scatt_table(i,k,3),k=1,10)
34          format(2X,11(F8.3,2X))
         enddo          
      endif
      close(31)      
    
      return
      end
