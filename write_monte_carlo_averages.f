CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C      WRITE AVERAGES FOR THE MONTE CARLO PART
C      (these averages correspond to one time step)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine write(nsim,iter,time,flag_write)
       include 'max_electron_number.h'
       parameter(n_val = 3)
        
       common
     &/ran_var/iso
     &/pi/pi,two_pi
     &/fund_const/q,h,kb,am0,eps_0  
     &/dri/qh
     &/mass/am(3),smh(3),hhm(3),tm(3),hm(3)
     &/nonp/af(3),af2(3),af4(3)
     &/variables/p(max_electron_number,8),ip(max_electron_number),
     1           energy(max_electron_number)
     
      integer nsim,iter
      real kb,time
      character*40 file_1,file_2
      character*40 file_3,file_4,file_5
      
      real nvaly(n_val)
      real velx_sum(n_val)
      real vely_sum(n_val)
      real velz_sum(n_val)
      real velocity_x(n_val)
      real velocity_y(n_val)
      real velocity_z(n_val)
      real sume(n_val)

C     Open output files

      file_1 = 'vx_time_averages'
      file_2 = 'vy_time_averages'
      file_3 = 'vz_time_averages'
      file_4 = 'energy_time_averages'
      file_5 = 'valley_occupation'
      open(unit=1,file=file_1,status='unknown')
      open(unit=2,file=file_2,status='unknown')
      open(unit=3,file=file_3,status='unknown')
      open(unit=4,file=file_4,status='unknown')   
      open(unit=5,file=file_5,status='unknown')

      do i = 1,n_val
         velx_sum(i) = 0.
         vely_sum(i) = 0.
         velz_sum(i) = 0.
         sume(i) = 0.
         nvaly(i) = 0   
      enddo

      do i = 1,nsim

         iv=ip(i)
         ee = energy(i)
         denom = 1./(1.+af2(iv)*ee)

c         velx = p(i,1)*denom
c         vely = p(i,2)*denom
c         velz = p(i,3)*denom

         if(iv.eq.1)then
            velx=hm(1)*p(i,1)*denom
            vely=hm(2)*p(i,2)*denom
            velz=hm(3)*p(i,3)*denom
         endif
         if(iv.eq.2)then
            velx=hm(2)*p(i,1)*denom
            vely=hm(1)*p(i,2)*denom
            velz=hm(3)*p(i,3)*denom
         endif
         if(iv.eq.3)then
            velx=hm(3)*p(i,1)*denom
            vely=hm(2)*p(i,2)*denom
            velz=hm(1)*p(i,3)*denom
         endif

         velx_sum(iv) = velx_sum(iv) + velx
         vely_sum(iv) = vely_sum(iv) + vely
         velz_sum(iv) = velz_sum(iv) + velz       
         sume(iv) = sume(iv) + energy(i)                   
         nvaly(iv) = nvaly(iv) + 1
          
       enddo
       
       do i = 1,n_val
          if(nvaly(i).ne.0)then
             velocity_x(i)=velx_sum(i)/nvaly(i)
             velocity_y(i)=vely_sum(i)/nvaly(i)
             velocity_z(i)=velz_sum(i)/nvaly(i)
             sume(i)=sume(i)/nvaly(i)
          endif                
       enddo

       if(flag_write.eq.0)then
          write(1,*)' time  vx_1  vx_2  vx_3'
          write(2,*)' time  vy_1  vy_2  vy_3'
          write(3,*)' time  vz_1  vz_2  vz_3'
          write(4,*)' time  Ek_1  Ek_2  Ek_3'
          write(5,*)' time  valley-1  valley-2  valley-3'
       endif

       if(mod(iter,4).eq.0)then        
	  write(1,88)time,velocity_x(1),
     1                velocity_x(2),velocity_x(3)
	  write(2,88)time,velocity_y(1),
     1                velocity_y(2),velocity_y(3)
	  write(3,88)time,velocity_z(1),
     1                velocity_z(2),velocity_z(3)
	  write(4,88)time,sume(1),sume(2),sume(3)
          write(5,88)time,nvaly(1),nvaly(2),nvaly(3)
88        format(2X,4(e12.6,4x))
       endif

       return
       end


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C      SAVE HISTOGRAMS
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine histograms(nsim,file_name)
       include 'max_electron_number.h'
       
       common
     &/variables/p(max_electron_number,8),ip(max_electron_number),
     1           energy(max_electron_number)

       integer nsim
       real kx,ky,kz,e
       character*30 file_name

       open(unit=6,file=file_name,status='unknown')
       write(6,*)'kx   ky   kz   energy'

       do i = 1, nsim
          kx = p(i,1)
	    ky = p(i,2)
	    kz = p(i,3)
	    e = energy(i)	      		 
          write(6,*)kx,ky,kz,e
       enddo
       close(6)

       return
       end              		
