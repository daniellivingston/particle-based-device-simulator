CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C       SELECT SCATTERING MECHANISM AND PERFORM 
C       THE SCATTERING PART THAT MODIFIES PARTICLE ATRIBUTES
C       (kx, ky, kz, iv, energy)    
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine scatter_carrier()
      include 'scattering_table_parameters.h'

       common
     &/ran_var/iso
     &/scatt_par/emax,de,w(n_scatt_max,n_doping_regions),
     1           tau_max(n_doping_regions),
     2           max_scatt_mech(n_doping_regions)
     &/scatt_par2/flag_mech(n_scatt_max,n_doping_regions),
     1           i_valley(10,3)
     &/table/scatt_table(n_lev,n_scatt_max,n_doping_regions)
     &/particle_atr/kx,ky,kz,iv,e,i_region,
     1              x_position,y_position,ix_fix,jy_fix
     &/frequency/freq(10,3)
      
      integer n_lev,iv,i_final
      real kx,ky,kz,e

C     Calculate index to the scattering table

      loc = e/de
      if(loc.eq.0)loc=1
      if(loc.gt.n_lev)loc=n_lev
	   
C     Select scattering mechanism

      i_top = max_scatt_mech(iv)
      rr = ran(iso)
      if(rr.ge.scatt_table(loc,i_top,iv))then
         freq(i_top+1,iv)=freq(i_top+1,iv)+1
         goto 222  ! self-scattering
      endif
      if(rr.lt.scatt_table(loc,1,iv))then
         i_fix = 1
         freq(i_fix,iv)=freq(i_fix,iv)+1
         goto 111
      endif
      if(i_top.gt.1)then
        do i=1,i_top-1
           bound_lower = scatt_table(loc,i,iv)
           bound_upper = scatt_table(loc,i+1,iv)
           if(rr.ge.bound_lower.and.rr.lt.bound_upper)then
              i_fix = i + 1
              freq(i_fix,iv)=freq(i_fix,iv)+1
              goto 111
           endif
        enddo
      endif

111   continue

C     Perform scattering (change energy and randomize momentum)    

      select_mech = flag_mech(i_fix,iv)
      i_final = i_valley(i_fix,iv)
      if(select_mech.eq.1)then
         call isotropic(i_fix,i_final)
      elseif(select_mech.eq.2)then
         call polar_optical_angle(i_fix)
      elseif(select_mech.eq.3)then
         call Coulomb_angle_BH()
      endif
      iv = i_final

222   continue

      return
      end
      

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C       SCATTERING SUBROUTINES
C       (CHANGE ENERGY AND WAVEVECTORS OF PARTICLES)
C
C       In the definition of the scattering rates, a variable called
C       'flag_mech' has been defined.  The values assigned to this
C       variable correspond to:
C       
C       1  ==>  Isotropic scattering (acoustic, intervalley)
C       2  ==>  Polar_optical ==> anisotropic scattering (small angle)
C       3  ==>  Coulomb scattering ==> small-angle scattering
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C       ISOTROPIC SCATTERING PROCESS
C       uniform probability density for scattering in all directions
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine isotropic(i_fix,i_final)
       include 'scattering_table_parameters.h'

       common
     &/ran_var/iso
     &/pi/pi,two_pi 
     &/mass/am(3),smh(3),hhm(3)
     &/nonp/af(3),af2(3),af4(3)
     &/scatt_par/emax,de,w(n_scatt_max,n_doping_regions),
     1           tau_max(n_doping_regions),
     2           max_scatt_mech(n_doping_regions)
     &/particle_atr/kx,ky,kz,iv,e,i_region,
     1              x_position,y_position,ix_fix,jy_fix

      integer iv,i_final
      real kx,ky,kz,e
      
C     Update carrier energy
      e = e + w(i_fix,iv)

C     Update carrier wavevector
      rknew = smh(i_final)*sqrt(e*(1.+af(i_final)*e))
      fi = two_pi*ran(iso)
      ct = 1.-2.*ran(iso)
      st = sqrt(1.-ct*ct)
      kx = rknew*st*cos(fi)
      ky = rknew*st*sin(fi)
      kz = rknew*ct   

      return
      end


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C      POLAR OPTICAL PHONONS SCATTERING ANGLE
C      Randomize the polar angle according to the notes
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine polar_optical_angle(i_fix)
      include 'scattering_table_parameters.h'

       common
     &/ran_var/iso
     &/pi/pi,two_pi 
     &/mass/am(3),smh(3),hhm(3)
     &/nonp/af(3),af2(3),af4(3)
     &/scatt_par/emax,de,w(n_scatt_max,n_doping_regions),
     1           tau_max(n_doping_regions),
     2           max_scatt_mech(n_doping_regions)
     &/particle_atr/kx,ky,kz,iv,e,i_region,
     1              x_position,y_position,ix_fix,jy_fix

      integer iv,i_fix
      real kx,ky,kz,e
      real k,kxy
      real kxp,kyp,kzp,kp
      
C     Update carrier energy
      enew = e + w(i_fix,iv)

C     Calculate the rotation angles
      kxy = sqrt(kx*kx+ky*ky)
      k = sqrt(kxy*kxy+kz*kz)
      cth0 = kz/k
      sth0 = kxy/k
      cfi0 = kx/kxy
      sfi0 = ky/kxy

C     Randomize momentum in the rotated coordinate system
      kp = smh(iv)*sqrt(enew*(1.+af(iv)*enew))
      ge = e*(1.+af(iv)*e)
      gnew = enew*(1.+af(iv)*enew)
      zeta = 2.*sqrt(ge*gnew)/(ge+gnew-2.*sqrt(ge*gnew))
      rr = ran(iso)
      cth = ((zeta+1.)-(2.*zeta+1)**rr)/zeta
      sth = sqrt(1.-cth*cth)
      fi = two_pi*ran(iso)
      cfi = cos(fi)
      sfi = sin(fi)
      kxp = kp*sth*cfi
      kyp = kp*sth*sfi
      kzp = kp*cth

C     Return back to the original coordinate system
      kx = kxp*cfi0*cth0-kyp*sfi0+kzp*cfi0*sth0
      ky = kxp*sfi0*cth0+kyp*cfi0+kzp*sfi0*sth0
      kz = -kxp*sth0+kzp*cth0

      e = enew   

      return
      end


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C      COULOMB SCATTERING ANGLE
C      Randomize the polar angle according to the expressions
C      derived in the notes
C      The assumption is that the scattering process is elastic
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine  Coulomb_angle_BH()
      include 'scattering_table_parameters.h'

       common
     &/ran_var/iso
     &/pi/pi,two_pi 
     &/mass/am(3),smh(3),hhm(3)
     &/nonp/af(3),af2(3),af4(3)
     &/scatt_par/emax,de,w(n_scatt_max,n_doping_regions),
     1           tau_max(n_doping_regions),
     2           max_scatt_mech(n_doping_regions)
     &/coulomb/doping_density(n_doping_regions),
     1         Energy_debye(n_doping_regions)
     &/particle_atr/kx,ky,kz,iv,e,i_region,
     1              x_position,y_position,ix_fix,jy_fix

      integer iv,i_fix
      real kx,ky,kz,e
      real k,kxy
      real kxp,kyp,kzp
      
C     Update carrier energy
c     enew = e + w(i_fix,iv)

C     Calculate the rotation angles
      kxy = sqrt(kx*kx+ky*ky)
      k = sqrt(kxy*kxy+kz*kz)
      cth0 = kz/k
      sth0 = kxy/k
      cfi0 = kx/kxy
      sfi0 = ky/kxy

C     Randomize momentum in the rotated coordinate system
      ge = e*(1.+af(iv)*e)
      rr = ran(iso)
      cth = 1. - 2*rr/(1.+4.*(1-rr)*ge/Energy_debye(iv))
      sth = sqrt(1.-cth*cth)
      fi = two_pi*ran(iso)
      cfi = cos(fi)
      sfi = sin(fi)
      kxp = k*sth*cfi
      kyp = k*sth*sfi
      kzp = k*cth

C     Return back to the original coordinate system
      kx = kxp*cfi0*cth0-kyp*sfi0+kzp*cfi0*sth0
      ky = kxp*sfi0*cth0+kyp*cfi0+kzp*sfi0*sth0
      kz = -kxp*sth0+kzp*cth0

c     e = enew   

      return
      end


