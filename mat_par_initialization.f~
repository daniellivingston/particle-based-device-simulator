CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      Subroutine for materials parameter initialization
C      (Material parameters correspond to Si)
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine mat_par_initialization()
       
       include 'scattering_table_parameters.h'

       common
     &/ran_var/iso
     &/pi/pi,two_pi
     &/fund_const/q,h,kb,am0,eps_0 
     &/dri/qh
     &/temp/tem,Vt
     &/mass/amd,amc,smh,hhm,tm(3),hm(3)
     &/nonp/af,af2,af4
     &/intrinsic_density/intrinsic_carrier_density
     &/debye/debye_length
     &/density/density,sound_velocity
     &/dielec_func/eps_sc,eps_oxide,gamma
     &/delta_Ec/delta_Ec
     &/scatt_par/emax,de,w(n_scatt_max,n_doping_regions),
     1           tau_max(n_doping_regions),
     2           max_scatt_mech(n_doping_regions)     
     &/time_1/dt,dtau,tot_time
     &/select_acouctic/acoustic_scattering
     &/select_Coulomb/Coulomb_scattering
     &/select_intervalley_1/intervalley_zero_g
     &/select_intervalley_2/intervalley_zero_f
     &/select_intervalley_3/intervalley_first_g
     &/select_intervalley_4/intervalley_first_f
     &/sigma_acoustic/sigma_acoustic
     &/doping_regions/n_region
     &/coulomb/doping_density(n_doping_regions),
     1         Energy_debye(n_doping_regions)
     &/Def_pot_1/DefPot_zero_g
     &/Def_pot_2/DefPot_zero_f
     &/Def_pot_3/DefPot_first_g
     &/Def_pot_4/DefPot_first_f
     &/interval_phonons_1/phonon_zero_g
     &/interval_phonons_2/phonon_zero_f
     &/interval_phonons_3/phonon_first_g
     &/interval_phonons_4/phonon_first_f
   
       real kb
       real nonparabolicity_factor
       real intrinsic_carrier_density

C      Define fundamental constants and general parameters

       iso=1345
       am0=9.11e-31
       h=1.05459e-34
       q=1.60219e-19
       qh=q/h  
       eps_0=8.85419e-12
       kb=1.38066e-23
       pi=4.*atan(1.0)
       two_pi = 2.*pi

C      Define time step and maximum simulation time

       dt=0.1e-15
C       tot_time = 4.0e-12
       tot_time = 10.0e-12

C      Set temperature 

       tem=300.
       Vt=kb*tem/q
       print*,'Thermal voltage = ',Vt
       print*,'  '

C      Define material parameters for Si and SiO2
 
       am_l = 0.91
       am_t = 0.19
       nonparabolicity_factor = 0.5
       eps_sc = 11.8
       eps_sc = eps_sc*eps_0
       eps_oxide = 3.9
       eps_oxide = eps_oxide*eps_0
       gamma = eps_oxide/eps_sc
       density = 2329.
       sound_velocity = 9040.
       intrinsic_carrier_density= 1.45e16
       delta_Ec = 0.575/Vt
       debye_length = sqrt(eps_sc*Vt/q/intrinsic_carrier_density)

C      Define parameters for the scattering table

       emax=1.0
       de=emax/float(n_lev)

c      Coulomb_scattering = 1
       acoustic_scattering = 1
       intervalley_zero_g = 1
       intervalley_zero_f = 1
c      intervalley_first_g = 1
c      intervalley_first_f = 1

       sigma_acoustic = 6.55  !  [eV]
       DefPot_zero_g = 5.23E10   ! [eV/m]
       DefPot_zero_f = 5.23E10   ! [eV/m]
       DefPot_first_g = 0.E10   ! [eV/m]
       DefPot_first_f = 0.E10   ! [eV/m]
   
       phonon_zero_g = 0.063  !  [eV]
       phonon_zero_f = 0.059  !  [eV]
       phonon_first_g = 0.0278  !  [eV]
       phonon_first_f = 0.029  !  [eV]

C      Map parameters into internal variables used in the code

       amd = (am_l*am_t*am_t)**(1./3.)
       amc = 3./(1./am_l + 2./am_t)
       tm(1) = sqrt(amc/am_l)
       tm(2) = sqrt(amc/am_t)
       tm(3) = sqrt(amc/am_t)
       amd = amd*am0
       amc = amc*am0
       hm(1) = h/amc*tm(1)
       hm(2) = h/amc*tm(2)
       hm(3) = h/amc*tm(3)

       af = nonparabolicity_factor
       af2 = 2.*af
       af4 = 4.*af
       smh = sqrt(2.*amc)*sqrt(q)/h
       hhm =h/amc/q*h/2.

       return
       end  
       
