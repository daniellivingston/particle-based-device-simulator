CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      MAIN PROGRAM FOR MONTE CARLO SOLUTION OF SILICON-BASED
C      SEMICONDUCTOR DEVICES
C
C      Important notes:
C
C      At the moment some of the subroutines are written for a
C      MOSFET device
C
       include 'scattering_table_parameters.h'
       include 'max_electron_number.h'
       include 'averaging_time.h'
       include 'max_grid_size.h'
C
C      n_lev  => # of energy levels in the scattering table
C      n_scatt_max => maximum # of scattering mechanisms
C      max_electron_number   => maximum number of simulated electrons
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

       common
     &/time_1/dt,dtau,tot_time
     &/temp/tem,Vt
     &/doping_regions/n_region
     &/input_voltages/V_source,V_drain,V_gate,V_substrate
     &/electrons_used/n_used
     &/drain_current/idd_out,idd_eli,idd_cre
     &/source_current/iss_out,iss_eli,iss_cre
     &/mesh_size/nx_max,ny_max
     &/potential/fai(0:nx_size,0:ny_size)
     &/potential2/fai2(0:nx_size,0:ny_size)

       character*40 file_name
       integer nsim

C     Initialize material parameters and device structure

       call mat_par_initialization()
       print*,'material parameters initialized'
       print*,'  '

       call device_structure_initialization()
       print*,'device structure initialized'
       print*,'  '

       call source_drain_carrier_number()
       print*,'Number of carriers for S and D regions calculated'
       print*,'  '

C     Calculate the scattering table
       call sc_table()
       print*,'scattering table calculated'
       print*,'  '

C     Read voltages form file
       open(unit=1,file='input_voltages',status='unknown')
       read(1,*)V_source_input
       read(1,*)V_drain_input
       read(1,*)V_gate_input
       read(1,*)V_substrate_input
       read(1,*)ax
       read(1,*)ay
       close(1)

C     Solve equilibrium Poisson equation
       V_source = 0.
       V_drain = 0.
       V_gate = V_gate_input
       V_substrate = 0.
       V_gate = V_gate/Vt

       call apply_voltage()
       call poisson_SOR(0.)
c       call quantum_correction(ax,ay)
       call electric_field_update()

C     Initialize electrons
       call electrons_initialization()
       call count_used_particles(ne)
       print*,'Number of electrons in use = ',ne
       print*,'  '

       call write_mesh()
       call write_electron_distribution()
       call write_electric_field()
       call write_potential(0.)
       print *,'Vt0:',Vt

C     Apply bias at the contacts
       V_source = V_source_input
       V_drain = V_drain_input
       V_substrate = V_substrate_input
       V_source = V_source/Vt
       V_drain = V_drain/Vt
       V_substrate = V_substrate/Vt
       call apply_voltage()

C     Start the Monte Carlo procedure
       time = 0.
       j = 0
       n_time_steps_av = int(averaging_time/dt)
       n_times = int(tot_time/dt)/n_time_steps_av
       iter_total = n_time_steps_av*n_times
       print*,'Number of time steps for averaging =',
     1        n_time_steps_av
       print*,'Total number of iterations = ',
     1        iter_total
       print*,'  '
       open(unit=33,file='charge_source_drain',status='unknown')

       do while(j.le.iter_total)
          j = j + 1

          jm = mod(j,10)
          if(jm.eq.0)then
             write(*,*)j
          endif
          time=dt*float(j)

          call free_flight_scatter()
c          call count_used_particles(ne)
c          print*,'Number of electrons in use = ',ne

          call check_source_drain_contacts()
c          call count_used_particles(ne)
c          print*,'Number of electrons in use = ',ne

          call delete_particles()
          call charge_assignment_NEC()
          call poisson_SOR(1.)
c          call quantum_correction(ax,ay)
c          print*,'quantum corr:',iter_total+1-j
       do ii=0,nx_max
       do ij=0,ny_max
          fai2(ii,ij)=fai(ii,ij)
       enddo
       enddo
          call electric_field_update()
          iter_reference = mod(j,n_time_steps_av)

          if(iter_reference.eq.0)then
             print*,'time = ',time
             call count_used_particles(ne)
             print*,time,idd_out,idd_eli,idd_cre
             print*,time,iss_out,iss_eli,iss_cre
             print*,'Number of electrons in use = ',ne
             print*,'  '
             call write_electron_distribution()
             call write_electric_field()
             call write_potential(1.)
          endif

          call velocity_energy_cumulative(iter_reference,
     1                                    n_time_steps_av)
          drain_factor = float(idd_out+idd_eli-idd_cre)
          source_factor = float(iss_out+iss_eli-iss_cre)
          write(33,4)time,source_factor,drain_factor
4         format(2X,3(e12.6,2X))

       enddo	! End of the time loop
       close(33)

222    continue

       end


