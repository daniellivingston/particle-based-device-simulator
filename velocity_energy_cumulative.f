CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      Calculate cumulative velocities and energies of the particles
C      Write averages in a file
C
C      Important note: This is device independent routine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC       
       subroutine velocity_energy_cumulative(iter_reference,
     1                                       n_time_steps_av)
       
       include 'max_electron_number.h'
       include 'max_grid_size.h'
       parameter(n_valley = 3)
        
       common
     &/fund_const/q,h,kb,am0,eps_0       
     &/mesh_size/nx_max,ny_max       
     &/mesh/mesh_sizex,mesh_sizey
     &/mass/am(3),smh(3),hhm(3),hm(3),tm(3)
     &/nonp/af(3),af2(3),af4(3)
     &/variables/p(max_electron_number,8),ip(max_electron_number),
     1           energy(max_electron_number)
     &/electrons_used/n_used
     &/vel_averages/velocity_x_sum(0:nx_size),
     1              velocity_y_sum(0:nx_size),
     2              velocity_z_sum(0:nx_size),
     3              current_sum(0:nx_size)
     &/energy_averages/energy_sum(0:nx_size)
     
       real mesh_sizex,mesh_sizey,kb
       integer n_used,nx_max,ny_max
       integer iter_reference,n_time_steps_av
       
       real nvaly(0:nx_size,n_valley)
       real velx_sum(0:nx_size,n_valley)
       real vely_sum(0:nx_size,n_valley)
       real sume(0:nx_size,n_valley)
       real el_number(0:nx_size)
       
       if(iter_reference.eq.0)then
       
          open(unit=1,file='velocity_energy_averages',
     1         status = 'unknown')
          do i = 0,nx_max
             write(1,*)velocity_x_sum(i)/
     1                      float(n_time_steps_av),
     2                 velocity_y_sum(i)/
     3                      float(n_time_steps_av),
     4                 energy_sum(i)/float(n_time_steps_av)
          enddo
          close(1)
          open(unit=1,file='current_averages',status='unknown')
          do i = 0,nx_max
             write(1,*)current_sum(i)/float(n_time_steps_av)
          enddo
          close(1)
                        
          do i = 0,nx_max
             velocity_x_sum(i) = 0.
             velocity_y_sum(i) = 0.
             velocity_z_sum(i) = 0.
             energy_sum(i) = 0.
             current_sum(i) = 0.
          enddo
          
       endif
       
       do i = 0,nx_max
          el_number(i) = 0
          do j = 1,n_valley
             nvaly(i,j) = 0
             velx_sum(i,j) = 0.
             vely_sum(i,j) = 0.
             sume(i,j) = 0.      
          enddo
       enddo
        
       do n = 1,n_used
       if(ip(n).ne.9)then
       
          i = int(p(n,5)/mesh_sizex+0.5)
          if(i.lt.0)i=0
          if(i.gt.nx_max)i=nx_max
          iv = ip(n)
          if(iv.gt.n_valley)iv=n_valley
          ee = energy(n)
          denom = 1./(1.+af2(iv)*ee)

c          velx = p(n,1)*denom
c          vely = p(n,2)*denom

          if(iv.eq.1)then
             velx = hm(1)*p(n,1)*denom
             vely = hm(2)*p(n,2)*denom
c             velz = hm(3)*p(n,3)*denom
          endif
          if(iv.eq.2)then
             velx = hm(2)*p(n,1)*denom
             vely = hm(1)*p(n,2)*denom
c             velz = hm(3)*p(n,3)*denom
          endif
          if(iv.eq.3)then
             velx = hm(3)*p(n,1)*denom
             vely = hm(2)*p(n,2)*denom
c             velz = hm(1)*p(n,3)*denom
          endif
          
c         print *,i,iv
          velx_sum(i,iv) = velx_sum(i,iv) + velx
          vely_sum(i,iv) = vely_sum(i,iv) + vely
c          velz_sum(i,iv) = velz_sum(i,iv) + velz       
          sume(i,iv) = sume(i,iv) + ee                   
          nvaly(i,iv) = nvaly(i,iv) + 1
          el_number(i) = el_number(i) + 1         
             
       endif      
       enddo
       
       do i = 0,nx_max 
       if(el_number(i).ne.0)then
          factor = 1./el_number(i)
          term = velx_sum(i,1)+velx_sum(i,2)+velx_sum(i,3)
          velocity_x_sum(i) = velocity_x_sum(i) + term*factor
          current_sum(i) = current_sum(i) + term*q/mesh_sizex
          term = vely_sum(i,1)+vely_sum(i,2)+vely_sum(i,3)
          velocity_y_sum(i) = velocity_y_sum(i) + term*factor
          term = (sume(i,1)+sume(i,2)+sume(i,3))*factor
          energy_sum(i) = energy_sum(i) + term
       endif    
       enddo

       return
       end
