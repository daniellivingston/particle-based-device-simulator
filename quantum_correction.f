CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C       CORRECT fai for QUANTUM EFFECTS
C
C            a = 0 (no modification)
C            0 (quantum correction,  a=5.0e-10 Ferry value)
C
C      Important note: This is device independent routine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      subroutine quantum_correction(ax,ay)
       include 'max_grid_size.h'


       common
     &/mesh_size/nx_max,ny_max
     &/potential/fai(0:nx_size,0:ny_size)
     &/potential2/fai2(0:nx_size,0:ny_size)
     &/electron_density/electron_density(0:nx_size,0:ny_size)
     &/debye/debye_length
     &/source_drain/n_s,n_d
     &/mesh/mesh_sizex,mesh_sizey
     &/width/device_width
     &/intrinsic_density/intrinsic_carrier_density
     &/grida/xxa(0:nx_size),yya(0:ny_size)
     &/grid/xx(-1:nx_size),yy(-1:ny_size)
     &/par/rad,ix,iy
      external func1d





      real number_density(0:nx_size,0:ny_size)
      real mesh_sizex,mesh_sizey
      real norm,intrinsic_carrier_density,kb


c     radius of integration limits:
     
      rad=4.0

      cell_volume = mesh_sizex*mesh_sizey*device_width
      norm = cell_volume*intrinsic_carrier_density

      if(ay.ne.0.0) then
      xxa(0)=0.0
      yya(0)=0.0
      do i = 1,nx_max
        if(ax.ne.0.0) then
        xxa(i)=xxa(i-1)+mesh_sizex/ax
        else
        xxa(i)=xxa(i-1)+mesh_sizex
        endif
      enddo
      do j = 1,ny_max
        yya(j)=yya(j-1)+mesh_sizey/ay
      enddo
      endif

      do ix = 0,nx_max
      do iy = 0,ny_max
      if(iy.lt.8) then
       if((ix.gt.n_s).and.(ix.lt.n_d)) then
        if(ay.ne.0.0) then
         if(ax.ne.0.0) then
         call QUAD2D(-rad,rad,SS)
         fai2(ix,iy)=SS
         else
         CALL QGAUSY(func1d,-rad,rad,SS)
         fai2(ix,iy)=SS
         endif
        else
         fai2(ix,iy)=fai(ix,iy)
        endif
       else
         fai2(ix,iy)=fai(ix,iy)
       endif
      else
         fai2(ix,iy)=fai(ix,iy)
      endif
      enddo
      enddo




      return
      end

      real function Vint(xs,ys)
       include 'max_grid_size.h'


       common
     &/mesh_size/nx_max,ny_max
     &/potential/fai(0:nx_size,0:ny_size)
     &/grida/xxa(0:nx_size),yya(0:ny_size)
     &/source_drain/n_s,n_d
     &/temp/tem,Vt



c     use bilinear interpolation
      do 635 ii=0,nx_max
       if((xs.ge.xxa(ii)).and.(xs.lt.xxa(ii+1))) then
        id=ii
        endif
 635   continue
        if(xs.ge.xxa(nx_max)) id=-1
        if(xs.lt.xxa(0)) id=-2

      do 645 jj=0,ny_max
       if((ys.ge.yya(jj)).and.(ys.lt.yya(jj+1))) then
        jd=jj
        endif
 645   continue
        if(ys.ge.yya(ny_max)) jd=-1
        if(ys.lt.yya(0)) jd=-2


       if((jd.gt.-1).and.(id.gt.-1)) then
       y1=fai(id,jd)
       y2=fai(id+1,jd)
       y3=fai(id+1,jd+1)
       y4=fai(id,jd+1)

       ti=(xs-xxa(id))/(xxa(id+1)-xxa(id))
       uj=(ys-yya(jd))/(yya(jd+1)-yya(jd))
       Vint=(1.0-ti)*(1.0-uj)*y1+ti*(1.0-uj)*y2+ti*uj*y3
     1            +(1.0-ti)*uj*y4
       else if((jd.gt.-1).and.(id.eq.-2)) then
        Vint=fai(0,jd)
       else if((jd.gt.-1).and.(id.eq.-1)) then
        Vint=fai(nx_max,jd)
       else if((jd.eq.-2).and.(id.gt.-1)) then
        if((id.le.n_s).or.(id.ge.n_d)) then
        Vint=fai(id,0)
        else
        Eox=(fai(id,1)-fai(id,0))/(yya(1)-yya(0))
        Vint=fai(id,0)-3.2/Vt+Eox*Vt*abs(ys-yya(0))
        endif
       else if((jd.eq.-1).and.(id.gt.-1)) then
        Vint=fai(id,ny_max)
       else if((jd.eq.-2).and.(id.eq.-2)) then
        Vint=fai(0,0)
       else if((jd.eq.-2).and.(id.eq.-1)) then
        Vint=fai(nx_max,0)
       else if((jd.eq.-1).and.(id.eq.-2)) then
        Vint=fai(0,ny_max)
       else if((jd.eq.-1).and.(id.eq.-1)) then
        Vint=fai(nx_max,ny_max)
       endif



      return
      end

      FUNCTION FUNC(X,Y)
      external Vint
       include 'max_grid_size.h'
       common
     &/grida/xxa(0:nx_size),yya(0:ny_size)
     &/par/rad,ix,iy
     &/pi/pi,two_pi

      arg=(x**2.+y**2.)/2.
      xp=xxa(ix)+x
      yp=yya(iy)+y
      func=exp(-arg)*Vint(xp,yp)/(2.*pi)
      RETURN
      END

      FUNCTION func1d(y)
      include 'max_grid_size.h'
       common
     &/grida/xxa(0:nx_size),yya(0:ny_size)
     &/par/rad,ix,iy
     &/pi/pi,two_pi
      arg=(y**2.)/2.
      xp=xxa(ix)
      yp=yya(iy)+y
      func1d=exp(-arg)*Vint(xp,yp)/sqrt(two_pi)
      RETURN
      END

      SUBROUTINE QUAD2D(X1,X2,SS)
      EXTERNAL H
      CALL QGAUSX(H,X1,X2,SS)
      RETURN
      END
 
      FUNCTION G(YY)
      EXTERNAL FUNC
      COMMON /XY/ X,Y
      Y=YY
      G=FUNC(X,Y)
      RETURN
      END
C
C
      FUNCTION H(XX)
      EXTERNAL Y1,Y2,G
      COMMON /XY/ X,Y
      X=XX
      CALL QGAUSY(G,Y1(X),Y2(X),SS)
      H=SS
      RETURN
      END

      FUNCTION Y1(X)
      common  /par/rad,ix,iy
      Y1=-sqrt(rad*rad-x*x)
      RETURN
      END

      FUNCTION Y2(X)
      common  /par/rad,ix,iy
      Y2=sqrt(rad*rad-x*x)
      RETURN
      END

      SUBROUTINE QGAUSX(FUNC,A,B,SS)
      EXTERNAL FUNC
      DIMENSION X(5),W(5)
      DATA X/.1488743389,.4333953941,.6794095682,.8650633666,.9739065285
     */
      DATA W/.2955242247,.2692667193,.2190863625,.1494513491,.0666713443
     */
      XM=0.5*(B+A)
      XR=0.5*(B-A)
      SS=0
      DO 11 J=1,5
        DX=XR*X(J)
        SS=SS+W(J)*(FUNC(XM+DX)+FUNC(XM-DX))
11    CONTINUE
      SS=XR*SS
      RETURN
      END

      SUBROUTINE QGAUSY(FUNC,A,B,SS)
      DIMENSION X(5),W(5)
      EXTERNAL FUNC
      DATA X/.1488743389,.4333953941,.6794095682,.8650633666,.9739065285
     */
      DATA W/.2955242247,.2692667193,.2190863625,.1494513491,.0666713443
     */
      XM=0.5*(B+A)
      XR=0.5*(B-A)
      SS=0
      DO 11 J=1,5
        DX=XR*X(J)
        SS=SS+W(J)*(FUNC(XM+DX)+FUNC(XM-DX))
11    CONTINUE
      SS=XR*SS
      RETURN
      END

