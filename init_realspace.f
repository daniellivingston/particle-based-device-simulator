CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      Initialize carrier position based on node location
C
C      Important note: This is device independent routine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC       
       subroutine init_realspace(ne,i,j)
       
       include 'max_electron_number.h'
        
       common
     &/ran_var/iso
     &/mesh_size/nx_max,ny_max
     &/mesh/mesh_sizex,mesh_sizey
     &/length/device_length
     &/depth/device_depth
     &/width/device_width
     &/variables/p(max_electron_number,8),ip(max_electron_number),
     1           energy(max_electron_number)
     
       real mesh_sizex,mesh_sizey
       integer nx_max,ny_max,i,j,ne
     
C      Define particle coordinates based on grid location

       if(i.ne.0.and.i.ne.nx_max)then
          x_position = (float(i)+ran(iso)-0.5)*mesh_sizex
          goto 11
       elseif(i.eq.0)then
          x_position = 0.5*ran(iso)*mesh_sizex
          goto 11
       elseif(i.eq.nx_max)then
          x_position = device_length-0.5*ran(iso)*mesh_sizex
       endif
11     continue
       if(x_position.lt.0)x_position=-x_position
       if(x_position.gt.device_length)then
          factor = abs(x_position - device_length)
          x_position = device_length - factor
       endif
       if(j.ne.0.and.j.ne.ny_max)then
          y_position = (float(j)+ran(iso)-0.5)*mesh_sizey
          goto 22
       elseif(j.eq.0)then
          y_position = 0.5*ran(iso)*mesh_sizey
          goto 22
       elseif(j.eq.ny_max)then
          y_position = device_depth-0.5*ran(iso)*mesh_sizey
       endif
22     continue
       if(y_position.lt.0)y_position=-y_position
       if(y_position.gt.device_depth)then
          factor = abs(y_position - device_depth)
          y_position = device_depth - factor
       endif

C      Map particle atributes
       
       p(ne,5) = x_position             
       p(ne,6) = y_position
	   
       return
       end
