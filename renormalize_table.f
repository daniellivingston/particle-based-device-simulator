CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C     Generic subroutine that renormalizes the scattering table
C     for a given valley
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine renormalize_table(iv,i_count)
      include 'scattering_table_parameters.h'

       common
     &/scatt_par/emax,de,w(n_scatt_max,n_doping_regions),
     1           tau_max(n_doping_regions),
     2           max_scatt_mech(n_doping_regions)
     &/table/scatt_table(n_lev,n_scatt_max,n_doping_regions)

      integer n_lev,iv,i_count

      max_scatt_mech(iv) = i_count

      if(max_scatt_mech(iv).ge.1)then

      if(max_scatt_mech(iv).gt.1)then
         do i = 2, max_scatt_mech(iv)
         do k = 1, n_lev
            scatt_table(k,i,iv) = scatt_table(k,i-1,iv) +
     1                            scatt_table(k,i,iv)
         enddo
         enddo
      endif

      i_max = max_scatt_mech(iv)
      tau = 0.
      do i = 1,n_lev
         if(scatt_table(i,i_max,iv).gt.tau)
     1                  tau = scatt_table(i,i_max,iv)
      enddo

      do i = 1, max_scatt_mech(iv)
      do k = 1, n_lev
         scatt_table(k,i,iv) = scatt_table(k,i,iv)/tau
      enddo
      enddo

      tau_max(iv) = 1./tau
      print*,'valley index = ',iv,'  tau_max =',tau_max
      print*,'iv = ',iv
      print*,'  '

      endif

      return
      end
