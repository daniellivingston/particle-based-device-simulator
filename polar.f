CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C     Subroutine for the calculation of the
C     POLAR OPTICAL PHONONS scattering rate
C     (absorption + emission)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine polar_rate(i_count,iv,w0,
     1                      out_file_1, out_file_2)
      include 'scattering_table_parameters.h'

       common
     &/pi/pi,two_pi 
     &/temp/tem,Vt
     &/fund_const/q,h,kb,am0,eps_0 
     &/dri/qh
     &/mass/am(3),smh(3),hhm(3)
     &/nonp/af(3),af2(3),af4(3)
     &/scatt_par/emax,de,w(n_scatt_max,n_doping_regions),
     1           tau_max(n_doping_regions),
     2           max_scatt_mech(n_doping_regions)
     &/scatt_par2/flag_mech(n_scatt_max,n_doping_regions),
     1           i_valley(10,3)
     &/table/scatt_table(n_lev,n_scatt_max,n_doping_regions)
     &/dielec_func/eps_high,eps_low,
     1             eps_sc,eps_oxide,gamma

      integer i_count,iv,n_lev
      real kb,w0
      character*30 out_file_1, out_file_2

C    Calculate constant
      
      rnq = 1./(exp(w0/Vt)-1.)
      const = qh*qh*q*w0*sqrt(am(iv)/2./q)/4./pi
     1      * (1./eps_high - 1./eps_low)

C     (a) Scattering rate - absorption

      i_count = i_count + 1
      open(unit=10, file=out_file_1, status='unknown')
      write(10,*)'energy  ',out_file_1
      polar_ab = rnq*const
      
      do i = 1, n_lev
         ee = de*float(i)
         ef = ee + w0
         ge = ee*(1.+af(iv)*ee)
         gf = ef*(1.+af(iv)*ef)
         rnum = sqrt(ge) + sqrt(gf)
         denom = sqrt(ge) - sqrt(gf)
         A = (2*(1.+af2(iv)*ee)*(1.+af(iv)*ef)
     1     + af(iv)*(ge+gf))**2.
         B = - af2(iv)*sqrt(ge*gf)
     1     * (4.*(1.+af(iv)*ee)*(1.+af(iv)*ef)
     1     + af(iv)*(ge+gf))
         C = 4.*(1.+af(iv)*ee)*(1.+af(iv)*ef)
     1     * (1.+af2(iv)*ee)*(1.+af2(iv)*ef)
         A = 4.
         C = 4.
         B = 0.
         factor = (1.+af2(iv)*ef)/sqrt(ge)
     1          * (A*log(abs(rnum/denom))+B)/C
         absorption = polar_ab*factor
         scatt_table(i,i_count,iv) = absorption
         write(10,*)ee,absorption
      enddo
      close(10)
             
      flag_mech(i_count,iv) = 2
      w(i_count,iv) = w0
      i_valley(i_count,iv) = iv

C     (b) Scattering rate - emission

      i_count = i_count + 1
      open(unit=11, file=out_file_2, status='unknown')
      write(11,*)'energy  ',out_file_2
      polar_em = (1.+rnq)*const
      
      do i = 1, n_lev
         ee = de*float(i)
         ef = ee - w0
         if(ef.le.0)then
            emission = 0
         else
            ge = ee*(1.+af(iv)*ee)
            gf = ef*(1.+af(iv)*ef)
            rnum = sqrt(ge) + sqrt(gf)
            denom = sqrt(ge) - sqrt(gf)
            A = (2*(1.+af2(iv)*ee)*(1.+af(iv)*ef)
     1        + af(iv)*(ge+gf))**2.
            B = - af2(iv)*sqrt(ge*gf)
     1        * (4.*(1.+af(iv)*ee)*(1.+af(iv)*ef)
     1        + af(iv)*(ge+gf))
            C = 4.*(1.+af(iv)*ee)*(1.+af(iv)*ef)
     1        * (1.+af2(iv)*ee)*(1.+af2(iv)*ef)
            A = 4.
            C = 4.
            B = 0.
            factor = (1.+af2(iv)*ef)/sqrt(ge)
     1             * (A*log(abs(rnum/denom))+B)/C
            emission = polar_em*factor
         endif
         scatt_table(i,i_count,iv) = emission
         write(11,*)ee,emission
      enddo
      close(11)
                    
      flag_mech(i_count,iv) = 2
      w(i_count,iv) = - w0
      i_valley(i_count,iv) = iv
      
      return
      end
