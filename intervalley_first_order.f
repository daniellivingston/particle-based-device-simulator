CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C     Generic subroutine for the calculation of 
C     INTERVALLEY PHONONS scattering rate
c     (absorption + emission)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine intervalley_first_order(i_count,iv,w0,
     1                        out_file_1,out_file_2)
       include 'scattering_table_parameters.h'
     
       common
     &/pi/pi,two_pi 
     &/temp/tem,Vt
     &/fund_const/q,h,kb,am0,eps_0 
     &/dri/qh
     &/mass/am(3),smh(3),hhm(3)
     &/nonp/af(3),af2(3),af4(3)
     &/scatt_par/emax,de,w(n_scatt_max,n_doping_regions),
     1           tau_max(n_doping_regions),
     2           max_scatt_mech(n_doping_regions)
     &/scatt_par2/flag_mech(n_scatt_max,n_doping_regions),
     1           i_valley(10,3)
     &/table/scatt_table(n_lev,n_scatt_max,n_doping_regions)
     &/density/density,sound_velocity
     &/intervalley1/coupling_constant
     &/intervalley2/delta_fi,final_valleys,i_final

      integer i_count,iv,n_lev
      real kb, w0
      character*30 out_file_1, out_file_2
     
C    Calculate constants

      rnq = 1./(exp(w0/Vt)-1.)
      final_mass = am(iv)
      const = final_valleys*sqrt(2.)*(coupling_constant**2.)
     1      * qh*qh*sqrt(q)/h/(pi*density*w0)
     2      * (final_mass/h)*final_mass*sqrt(final_mass)

C     (a) Scattering rate - absorption

      i_count = i_count + 1
      open(unit=10, file=out_file_1, status='unknown')
      write(10,*)'energy  ',out_file_1
      ab = rnq*const
      
      do i = 1, n_lev
         ee = de*float(i)
         ef = ee + w0 - delta_fi
         ge = ee*(1.+af(iv)*ee)
         gf = ef*(1.+af(iv)*ef)
         if(ef.le.0)then
            absorption = 0.
         else
            factor = sqrt(gf)*(ge+gf)*(1.+af2(iv)*ef)
            absorption = ab*factor
         endif
         scatt_table(i,i_count,iv) = absorption
         write(10,*)ee,absorption
      enddo
      close(10)
              
      flag_mech(i_count,iv) = 1
      w(i_count,iv) = w0 - delta_fi
      i_valley(i_count,iv) = i_final

C     (b) Scattering rate - emission

      i_count = i_count + 1
      open(unit=11, file=out_file_2, status='unknown')
      write(11,*)'energy  ',out_file_2
      em = (1.+rnq)*const
      
      do i = 1, n_lev
         ee = de*float(i)
         ef = ee - w0 - delta_fi
         ge = ee*(1.+af(iv)*ee)
         gf = ef*(1.+af(iv)*ef)
         if(ef.le.0)then
            emission = 0.
         else
            factor = sqrt(gf)*(ge+gf)*(1.+af2(iv)*ef)
            emission = em*factor
         endif
         scatt_table(i,i_count,iv) = emission
         write(11,*)ee,emission
      enddo
      close(11)
            
      flag_mech(i_count,iv) = 1
      w(i_count,iv) = - w0 - delta_fi     
      i_valley(i_count,iv) = i_final

      return
      end
