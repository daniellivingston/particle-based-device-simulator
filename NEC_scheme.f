CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     implementation of the NEC method for electric field calculation
C     at particle's location
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine local_field_value_NEC(fx,fy,fz)
       include 'max_grid_size.h'
       
       common
     &/mesh_size/nx_max,ny_max
     &/force/fx_field(0:nx_size,0:ny_size),
     1       fy_field(0:nx_size,0:ny_size)
     &/particle_atr/kx,ky,kz,iv,e,i_region,
     1              x_position,y_position,ix_fix,jy_fix
         
       real kx,ky,kz,e,x_position,y_position
       real fx,fy,fz
       integer iv,i_region,ix_fix,jy_fix,ix,jy

       ix = ix_fix
       jy = jy_fix
       if(ix.ge.nx_max)ix=nx_max-1
       if(jy.ge.ny_max)jy=ny_max-1
       if(ix.lt.0)ix=0
       if(jy.lt.0)jy=0
       fx=0.5*(fx_field(ix,jy)+fx_field(ix,jy+1))
       fy=0.5*(fy_field(ix,jy)+fy_field(ix+1,jy))

       return
       end


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     implementation of the NEC method for the charge assignment at
C     the node points
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine charge_assignment_NEC()

       include 'max_electron_number.h'
       include 'max_grid_size.h'

       common
     &/mesh/mesh_sizex,mesh_sizey
     &/mesh_size/nx_max,ny_max
     &/intrinsic_density/intrinsic_carrier_density
     &/electrons_used/n_used
     &/width/device_width
     &/variables/p(max_electron_number,8),ip(max_electron_number),
     1           energy(max_electron_number)
     &/electron_density/electron_density(0:nx_size,0:ny_size)

       integer n,n_used
       real mesh_sizex,mesh_sizey,norm_factor
       real intrinsic_carrier_density
       real elec_number(0:nx_size,0:ny_size)

C     Evaluate multiplication constant

       cell_volume = mesh_sizex*mesh_sizey*device_width
       norm_factor = cell_volume*intrinsic_carrier_density
       norm_factor = 1./norm_factor

C     Reset the charge vector

       do i = 0,nx_max
       do j = 0,ny_max
          elec_number(i,j) = 0.
       enddo
       enddo

C     Charge assignment part

       do n = 1,n_used
          if(ip(n).ne.9)then
             i=int(p(n,5)/mesh_sizex)
             j=int(p(n,6)/mesh_sizey)
             if(i.lt.0)i=0
             if(i.ge.nx_max)i=nx_max-1
             if(j.lt.0)j=0
             if(j.ge.ny_max)j=ny_max-1
             elec_number(i,j)=elec_number(i,j)+0.25
             elec_number(i,j+1)=elec_number(i,j+1)+0.25
             elec_number(i+1,j)=elec_number(i+1,j)+0.25
             elec_number(i+1,j+1)=elec_number(i+1,j+1)+0.25
          endif      
       enddo

C     Calculate electron density

       do i = 0,nx_max
       do j = 0,ny_max
          denn = elec_number(i,j)
          if(i.eq.0.or.i.eq.nx_max)denn = 2.*denn
          if(j.eq.0.or.j.eq.ny_max)denn = 2.*denn
          electron_density(i,j) = denn*norm_factor
       enddo
       enddo

       return
       end

