CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      Subroutine for materials parameter initialization
C      (Material parameters correspond to Si)
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine mat_par_initialization()
       include 'scattering_table_parameters.h'

       common
     &/ran_var/iso
     &/pi/pi,two_pi
     &/fund_const/q,h,kb,am0,eps_0 
     &/dri/qh
     &/temp/tem,Vt
     &/mass/am(3),smh(3),hhm(3),hm(3),tm(3)
     &/nonp/af(3),af2(3),af4(3)
     &/intrinsic_density/intrinsic_carrier_density
     &/debye/debye_length
     &/density/density,sound_velocity
     &/dielec_func/eps_high,eps_low,
     1           eps_sc,eps_oxide,gamma
     &/delta_Ec/delta_Ec
     &/scatt_par/emax,de,w(n_scatt_max,n_doping_regions),
     1           tau_max(n_doping_regions),
     2           max_scatt_mech(n_doping_regions)     
     &/time_1/dt,dtau,tot_time
     &/select_acouctic/acoustic_gamma,acoustic_L,acoustic_X
     &/select_Coulomb/Coulomb_scattering
     &/select_polar/polar_gamma,polar_L,polar_X
     &/select_intervalley_1/intervalley_gamma_L
     &/select_intervalley_2/intervalley_gamma_X
     &/select_intervalley_3/intervalley_L_gamma
     &/select_intervalley_4/intervalley_L_L
     &/select_intervalley_5/intervalley_L_X
     &/select_intervalley_6/intervalley_X_gamma
     &/select_intervalley_7/intervalley_X_L
     &/select_intervalley_8/intervalley_X_X
     &/sigma_acoustic/sigma_gamma,sigma_L,sigma_X
     &/polar_en/polar_en_gamma,polar_en_L,polar_en_X
     &/doping_regions/n_region
     &/coulomb/doping_density(n_doping_regions),
     1         Energy_debye(n_doping_regions)
     &/Def_pot_1/DefPot_gamma_L,DefPot_gamma_X
     &/Def_pot_2/DefPot_L_gamma,DefPot_L_L,DefPot_L_X
     &/Def_pot_3/DefPot_X_gamma,DefPot_X_L,DefPot_X_X
     &/interval_phonons_1/phonon_gamma_L,phonon_gamma_X
     &/interval_phonons_2/phonon_L_gamma,phonon_L_L,phonon_L_X
     &/interval_phonons_3/phonon_X_gamma,phonon_X_L,phonon_X_X

       real kb
       real intrinsic_carrier_density
       real nonparabolicity_gamma
       real nonparabolicity_L
       real nonparabolicity_X

C      Define fundamental constants and general parameters

       iso=1345
       am0=9.11e-31
       h=1.05459e-34
       q=1.60219e-19
       qh=q/h  
       eps_0=8.85419e-12
       kb=1.38066e-23
       pi=4.*atan(1.0)
       two_pi = 2.*pi

C      Define time step and maximum simulation time

       dt=0.1e-15
       tot_time = 4.0e-12

C      Set temperature 

       tem=300.
       Vt=kb*tem/q
       print*,'Thermal voltage = ',Vt
       print*,'  '
c       doping_density = 1.e21

C      Set the electric field

c       fx = 0.
c       fy = 7.e5
c       fz = 0.

C      Define Masses for different valleys

       rel_mass_gamma = 0.063
       rel_mass_L = 0.170
       rel_mass_X = 0.58

C      Define non-parabolicity factors

       nonparabolicity_gamma = 0.62
       nonparabolicity_L = 0.50
       nonparabolicity_X = 0.30

C      Define valley splitting and equivalent valleys

       split_L_gamma = -0.14 ! See Vasileska paper
       split_X_gamma = -0.05 ! See Vasileska paper
       eq_valleys_gamma = 1. ! changed
       eq_valleys_L = 4.
       eq_valleys_X = 3.

C      Define low-ferquency and high-frequency dielectric constants

C      Dielectric constant for Ge
C            Source: Ioffe

       eps_high = 16.2
       eps_low = 16.2
       eps_high = eps_high*eps_0
       eps_low = eps_low*eps_0

C      Define crystal density and sound velocity
C             Source: Aubrey-Fortuna

       density = 5327.
       sound_velocity = 5.4E3

C      Define material parameters for Si and SiO2
C             Source: Ioffe

       am_l = 1.59 ! changed
       am_t = 0.0815 ! changed
       nonparabolicity_factor = 0.3
       eps_sc = 11.8
       eps_sc = eps_sc*eps_0
       eps_oxide = 3.9
       eps_oxide = eps_oxide*eps_0
       gamma = eps_oxide/eps_sc
c       density = 2329.
c       sound_velocity = 9040.
c       intrinsic_carrier_density= 1.45e16
       intrinsic_carrier_density= 2.4e19 ! source: Virginia Semiconductor
       delta_Ec = 0.575/Vt
       debye_length = sqrt(eps_sc*Vt/q/intrinsic_carrier_density)

C      Define parameters for the scattering table

       emax=1.0
       de=emax/float(n_lev)

C      Select scattering mechanisms

c      Coulomb_scattering = 1
       acoustic_gamma = 1
       acoustic_L = 1
       acoustic_X = 1
       polar_gamma = 1
       polar_L = 1
       polar_X = 1
       intervalley_gamma_L = 1
       intervalley_gamma_X = 1
       intervalley_L_gamma = 1
       intervalley_L_L = 1
       intervalley_L_X = 1
       intervalley_X_gamma = 1
       intervalley_X_L = 1
       intervalley_X_X = 1

C      Define coupling constants
C          Source: Aubrey-Fortuna

       sigma_gamma = 5.0  !  [eV]
       sigma_L = 11.0  !  [eV]
       sigma_X = 9.0  !  [eV]

       polar_en_gamma = 0.037  !  [eV]
       polar_en_L = 0.037  !  [eV]
       polar_en_X = 0.037  !  [eV]

       DefPot_gamma_L = 2.0E10   ! [eV/m]
       DefPot_gamma_X = 1.0E11   ! [eV/m]
       DefPot_L_gamma = 2.0E10   ! [eV/m]
       DefPot_L_L = 3.0E10   ! [eV/m]
       DefPot_L_X = 4.06E10   ! [eV/m]
       DefPot_X_gamma = 1.0E11   ! [eV/m]
       DefPot_X_L = 4.06E10   ! [eV/m]
       DefPot_X_X = 9.46E10   ! [eV/m]

       phonon_gamma_L = 0.0276  !  [eV]
       phonon_gamma_X = 0.0276  !  [eV]
       phonon_L_gamma = 0.0276  !  [eV]
       phonon_L_L = 0.02756  !  [eV]
       phonon_L_X = 0.0276  !  [eV]
       phonon_X_gamma = 0.0276  !  [eV]
       phonon_X_L = 0.0276  !  [eV]
       phonon_X_X = 0.03704  !  [eV]

C      Map parameters into internal variables used in the code

C      CHANGE BELOW -- SEE CODE
       am(1) = rel_mass_gamma*am0
       am(2) = rel_mass_L*am0
       am(3) = rel_mass_X*am0
       af(1) = nonparabolicity_gamma
       af(2) = nonparabolicity_L
       af(3) = nonparabolicity_X

c       am(1) = rel_mass_gamma*am0
c       am(2) = rel_mass_L*am0
c       am(3) = rel_mass_X*am0
c       af(1) = nonparabolicity_gamma
c       af(2) = nonparabolicity_L
c       af(3) = nonparabolicity_X

c      OLD METHOD -- DELETE AND REPLACE

       amd_Si = (am_l*am_t*am_t)**(1./3.)
       amc_Si = 3./(1./am_l + 2./am_t)

       tm(1) = sqrt(amc_Si/am_l)
       tm(2) = sqrt(amc_Si/am_t)
       tm(3) = sqrt(amc_Si/am_t)

       amd_Si = amd_Si*am0
       amc_Si = amc_Si*am0

       am(1) = amc_Si
       am(2) = amc_Si
       am(3) = amc_Si

       af(1) = nonparabolicity_factor
       af(2) = nonparabolicity_factor
       af(3) = nonparabolicity_factor

c      END OF OLD METHOD

c       tm(1) = sqrt((am(i)/am0)/rel_mass_gamma)
c       tm(2) = sqrt((am(i)/am0)/rel_mass_L)
c       tm(3) = sqrt((am(i)/am0)/rel_mass_X)

       do i = 1,3
          hm(i) = h/am(i)*tm(i)
          smh(i)=sqrt(2.*am(i))*sqrt(q)/h
          hhm(i)=h/am(i)/q*h/2.
          af2(i) = 2.*af(i)
          af4(i) = 4.*af(i)
cc         print*,af(i),af2(i),af4(i)
       enddo

       return
       end

