CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C    Subroutine for the calculation of acoustic phonons
C    scattering rate
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine acoustic_rate(i_count,iv,out_file)
      include 'scattering_table_parameters.h'

       common
     &/pi/pi,two_pi 
     &/temp/tem,Vt
     &/fund_const/q,h,kb,am0,eps_0 
     &/dri/qh
     &/mass/am(3),smh(3),hhm(3)
     &/nonp/af(3),af2(3),af4(3)
     &/scatt_par/emax,de,w(n_scatt_max,n_doping_regions),
     1           tau_max(n_doping_regions),
     2           max_scatt_mech(n_doping_regions)
     &/scatt_par2/flag_mech(n_scatt_max,n_doping_regions),
     1           i_valley(10,3)
     &/table/scatt_table(n_lev,n_scatt_max,n_doping_regions)
     &/density/density,sound_velocity
     &/acoustic/sigma
     
      integer i_count,iv,n_lev
      real kb
      character*30 out_file

C    Calculate constant

      c_l = density*sound_velocity*sound_velocity
      const = sqrt(2.)*kb*tem/pi/h/c_l
     1      * (am(iv)/h)*(sqrt(am(iv))/h*sqrt(q))*(qh*q)
     2      * sigma*sigma

C    Create scattering table
                 
      i_count = i_count + 1
      open(unit = 10, file=out_file, status='unknown')
      write(10,*)'energy  ',out_file
      do i = 1, n_lev
         ee = de*float(i)
         fe = ee*(1.+af(iv)*ee)
         acoustic = const*sqrt(fe)*(1.+af2(iv)*ee)
         scatt_table(i,i_count,iv) = acoustic
         write(10,*)ee,acoustic
      enddo
      close(10)
      
      flag_mech(i_count,iv) = 1
      w(i_count,iv) = 0.
      i_valley(i_count,iv) = iv

      return
      end
