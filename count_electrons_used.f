CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C       Count the number of used electrons    
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine count_used_particles(ne)

       include 'max_electron_number.h'
        
       common
     &/variables/p(max_electron_number,8),ip(max_electron_number),
     1           energy(max_electron_number)

       integer ne
       
       ne = 0
       do i=1,max_electron_number
          if(ip(i).ne.9)ne = ne + 1
       enddo
       
       return
       end

        