CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     PERFORM THE K-SPACE AND REAL-SPACE MOTION OF THE CARRIERS
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine drift(tau)
       
       common
     &/pi/pi,two_pi 
     &/dri/qh
     &/temp/tem,Vt
     &/mass/am(3),smh(3),hhm(3),hm(3),tm(3)
     &/nonp/af(3),af2(3),af4(3)
     &/force/fx,fy,fz
     &/mesh/mesh_sizex,mesh_sizey
     &/inv_mesh/inv_mesh_sizex,inv_mesh_sizey
     &/mesh_size/nx_max,ny_max
     &/particle_atr/kx,ky,kz,iv,e,i_region,
     1              x_position,y_position,ix_fix,jy_fix

       real kx,ky,kz,e,tau,kxf,kyf,kzf
       real qh1,dkx,dky,dkz,x_position,y_position
       real mesh_sizex,inv_mesh_sizex
       real mesh_sizey,inv_mesh_sizey
       integer iv,i_region,ix_fix,jy_fix
       real fx,fy,fz

C      Calculate electric field

       call local_field_value_NEC(fx,fy,fz)

C      Update particle momentum and energy

       qh1 = qh*tau
       dkx = -qh1*fx
       dky = -qh1*fy
       dkz = -qh1*fz

       kxf = kx+dkx
       kyf = ky+dky
       kzf = kz+dkz

       skx = kxf*kxf
       sky = kyf*kyf
       skz = kzf*kzf
       sk = skx+sky+skz
       gk = hhm(iv)*sk
       e = 2*gk/(1.+sqrt(1.+af4(iv)*gk))

C      Update particle's position using the 'leap-frog' scheme

       avkx = 0.5*(kx+kxf)
       avky = 0.5*(ky+kyf)
       avkz = 0.5*(kz+kzf)

       kx=kxf
       ky=kyf
       kz=kzf

       denom = tau/(1.+af2(iv)*e)
c       dx=avkx*denom
c       dy=avky*denom

       if(iv.eq.1)then
       dx=hm(1)*avkx*denom
       dy=hm(2)*avky*denom
c         dz=hm(3)*avkz*denom
       elseif(iv.eq.2)then
       dx=hm(2)*avkx*denom
       dy=hm(1)*avky*denom
c         dz=hm(3)*avkz*denom
       elseif(iv.eq.3)then
       dx=hm(3)*avkx*denom
       dy=hm(2)*avky*denom
c         dz=hm(1)*avkz*denom
       endif

       x_position = x_position + dx
       y_position = y_position + dy
c      z_position = z_position + dz

C     Check simulation boundaries

       call check_boundary()

       if(iv.ne.9)then
          ix_fix = int(x_position/mesh_sizex)
       if(ix_fix.gt.nx_max)ix_fix=nx_max
          jy_fix = int(y_position/mesh_sizey)
       if(jy_fix.gt.ny_max)jy_fix=ny_max
          i_region = find_region(ix_fix,jy_fix)
       endif

       return
       end





