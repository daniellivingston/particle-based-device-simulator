CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      Calculate the electric field at the half-mesh point distance
C    
C      Important note: This subroutine is device independent
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine electric_field_update()     
       include 'max_grid_size.h'

       common
     &/temp/tem,Vt
     &/mesh/mesh_sizex,mesh_sizey
     &/inv_mesh/inv_mesh_sizex,inv_mesh_sizey
     &/mesh_size/nx_max,ny_max
     &/potential2/fai2(0:nx_size,0:ny_size)
     &/force/fx_field(0:nx_size,0:ny_size),
     1       fy_field(0:nx_size,0:ny_size)
  
      real mesh_sizex,inv_mesh_sizex
      real mesh_sizey,inv_mesh_sizey
      integer nx_max,ny_max
      inv_mesh_sizex = 1./mesh_sizex
      inv_mesh_sizey = 1./mesh_sizey

      do i = 0,nx_max-1
      do j = 0,ny_max
         fx_field(i,j)=(fai2(i,j)-fai2(i+1,j))/mesh_sizex*Vt
      enddo
      enddo

      do i = 0,nx_max
      do j = 0,ny_max-1        
         fy_field(i,j)=(fai2(i,j)-fai2(i,j+1))/mesh_sizey*Vt
      enddo
      enddo

      return
      end


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      Write the electric field:
C
C          => x-component of the field
C          => y-component of the field
C    
C      Important note: This subroutine is device independent
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine write_electric_field()    
       include 'max_grid_size.h'

       common
     &/mesh_size/nx_max,ny_max
     &/potential2/fai2(0:nx_size,0:ny_size)
     &/force/fx_field(0:nx_size,0:ny_size),
     1       fy_field(0:nx_size,0:ny_size)

       open(unit=1,file='Electric_field_x_component',
     1             status='unknown')
       do i = 0,nx_max
       do j = 0,ny_max
          write(1,*)fx_field(i,j)
       enddo
       enddo
       close(1)

       open(unit=1,file='Electric_field_y_component',
     1             status='unknown')
       do i = 0,nx_max
       do j = 0,ny_max
          write(1,*)fy_field(i,j)
       enddo
       enddo
       close(1)

       return
       end

