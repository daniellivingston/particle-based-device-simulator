CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C     Subroutine for the calculation of COULOMB SCATTERING rate 
C     Assumption ==> elastic scattering process
c     (Brooks-Herring approach)
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine Coulomb_BH(i_count,iv,out_file_1)
       include 'scattering_table_parameters.h'

       common
     &/pi/pi,two_pi 
     &/temp/tem,Vt
     &/fund_const/q,h,kb,am0,eps_0 
     &/dri/qh
     &/mass/am(3),smh(3),hhm(3)
     &/nonp/af(3),af2(3),af4(3)
     &/scatt_par/emax,de,w(n_scatt_max,n_doping_regions),
     1           tau_max(n_doping_regions),
     2           max_scatt_mech(n_doping_regions)
     &/scatt_par2/flag_mech(n_scatt_max,n_doping_regions),
     1           i_valley(10,3)
     &/table/scatt_table(n_lev,n_scatt_max,n_doping_regions)
     &/dielec_func/eps_high,eps_low,
     1             eps_sc,eps_oxide,gamma
     &/coulomb/doping_density(n_doping_regions),
     1         Energy_debye(n_doping_regions)
      
      integer i_count,iv,n_lev
      real kb
      character*30 out_file_1
     
C    Calculate constants

      Debye_length = sqrt(eps_high*Vt/q/
     1               doping_density(iv))
      Energy_debye(iv) = hhm(iv)/
     1               Debye_length/Debye_length
      final_mass = am(iv)
      factor = Debye_length*Debye_length/eps_sc
      const = doping_density(iv)*final_mass*qh
     1      * sqrt(2.*final_mass)/pi*qh
     2      * factor*qh*factor*qh*sqrt(q)   

C    Calculate scattering rate:

      i_count = i_count + 1
      open(unit=10, file=out_file_1, status='unknown')
      write(10,*)'energy  ',out_file_1
      
      do i = 1, n_lev
         ee = de*float(i)
         ge = ee*(1.+af(iv)*ee)
         factor = sqrt(ge)*(1.+af2(iv)*ee)
     1          / (1.+4.*ge/Energy_debye(iv))
         scatt_rate = const*factor         
         scatt_table(i,i_count,iv) = scatt_rate
         write(10,*)ee,scatt_rate
      enddo
      close(10)
              
      flag_mech(i_count,iv) = 3
      w(i_count,iv) = 0.
      i_valley(i_count,iv) = iv

      return
      end
