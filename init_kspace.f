CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      INITIALIZE CARRIER ENERGY AND WAVEVECTOR ACCORDING TO THE
C      MAXWELL-BOLTZMANN STATISTICS
C
C      Important note: This is device independent routine

C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC       
       subroutine init_kspace(ne,i,j)

       include 'scattering_table_parameters.h'       
       include 'max_electron_number.h'
        
       common
     &/ran_var/iso
     &/pi/pi,two_pi  
     &/temp/tem,Vt
     &/mass/am(3),smh(3),hhm(3)
     &/nonp/af(3),af2(3),af4(3)
     &/mesh_size/nx_max,ny_max
     &/source_drain/n_s,n_d  
     &/scatt_par/emax,de,w(n_scatt_max,n_doping_regions),
     1           tau_max(n_doping_regions),
     2           max_scatt_mech(n_doping_regions)
     &/variables/p(max_electron_number,8),ip(max_electron_number),
     1           energy(max_electron_number)
     
       real k,kx,ky,kz,e
       integer iv,i_region,ne,i,j
       
C      Set particle energy

       e = -(1.5*Vt)*log(ran(iso))

c      Set initial valley index and region
	
       rr = 3.*ran(iso)	
       if(rr.le.1.)then
          iv=1
       elseif(rr.le.2.)then  
	      iv=2
       elseif(rr.le.3.)then
          iv=3
       endif
       i_region = find_region(i,j)

C      Initial wavevector

       k=smh(iv)*sqrt(e*(1.+af(iv)*e))
       fai=two_pi*ran(iso)
       ct=1.-2.*ran(iso)
       st=sqrt(1.-ct*ct)
       kx=k*st*cos(fai)
       ky=k*st*sin(fai)
       kz=k*ct
       
C      Check boundaries

       if(i.eq.0.and.kx.lt.0)kx = -kx
       if(i.eq.nx_max.and.kx.gt.0)kx = -kx
       if(j.eq.0.and.ky.lt.0)ky = -ky
       if(j.eq.ny_max.and.ky.gt.0)ky = -ky    

C      Initial free-flight

103    rr = ran(iso)
       if(rr.le.1.e-5)go to 103
       tc = -(log(rr))*tau_max(iv)

C      Map particle atributes
       
       p(ne,1) = kx             
       p(ne,2) = ky
       p(ne,3) = kz
       p(ne,4) = tc
       p(ne,8) = i_region
       ip(ne) = iv
       energy(ne) = e
	   
       return
       end
