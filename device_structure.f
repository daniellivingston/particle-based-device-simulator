CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      SUBROUTINE FOR DEVICE STRUCTURE INITIALIZATION:
C
C         (1) defines source, drain and gate regions
C         (2) defines different doping regions
C         (3) defines mesh size
C         (4) defines initial doping and initial potential
C         (5) sets flags for various regions
C
C                 => flag_dom = 1 (ohmic contact)
C                 => flag_dom = 2 (semiconductor/oxide interface)
C                 => flag_dom = 3 (bulk region)
C
C         (6) calculates coefficients for Poisson equation solution
C
C      Important note: This is MOSFET specific subroutine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine device_structure_initialization()

       include 'scattering_table_parameters.h'
       include 'max_grid_size.h'

       common
     &/temp/tem,Vt
     &/debye/debye_length
     &/doping_regions/n_region
     &/mesh_size/nx_max,ny_max
     &/source_drain/n_s,n_d
     &/mesh/mesh_sizex,mesh_sizey
     &/conv_parameters/omega,tolerance
     &/oxide_thickness/oxide_thickness
     &/flag_domain/flag_dom(0:nx_size,0:ny_size)
     &/coulomb/doping_density(n_doping_regions),
     1         Energy_debye(n_doping_regions)
     &/length/device_length
     &/depth/device_depth
     &/width/device_width

       real mesh_sizex,mesh_sizey
       integer nx_max,ny_max,n_region

       open(unit=1,file='device_dimensions',status='unknown')
       read(1,*)source_drain_length
       read(1,*)source_drain_depth
       read(1,*)gate_length
       read(1,*)bulk_depth
       read(1,*)device_width
       read(1,*)oxide_thickness
       read(1,*)mesh_sizex
       read(1,*)mesh_sizey
       read(1,*)n_region
       read(1,*)doping_density(1)
       read(1,*)doping_density(2)
       read(1,*)doping_density(3)
       read(1,*)doping_density(4)
       read(1,*)omega
       read(1,*)tolerance
       close(1)

       tolerance = tolerance/Vt
       oxide_thickness = oxide_thickness/debye_length

C     Specify reference points for source and drain regions and junction depth

       n_s = int(source_drain_length/mesh_sizex)
       n_d = int((source_drain_length+gate_length)/mesh_sizex)
       n_j = int(source_drain_depth/mesh_sizey)
          print*,'n_s, nd:',n_s,n_d

       nx_max = int((2.*source_drain_length+gate_length)/mesh_sizex+0.5)
       if(nx_max.gt.nx_size)then
          print*,'x-mesh size exceeds maximum size'
          pause
       endif
       device_length = float(nx_max)*mesh_sizex

       ny_max = int((source_drain_depth+bulk_depth)/mesh_sizey+0.5)
       if(ny_max.gt.ny_size)then
          print*,'y-mesh size exceeds maximum size'
          pause
       endif
       device_depth = float(ny_max)*mesh_sizey

       print*,'Mesh size based on device dimensions:'
       print*,'nx_max=',nx_max,'  ny_max=',ny_max
       print*,'  '

C     Specify flags for Poisson equation solution

C     (a) bulk
        do i = 0,nx_max
        do j = 0,ny_max
            flag_dom(i,j) = 3
        enddo
        enddo

C     (b) ohmic contacts
        j = 0
        do i = 0,n_s
            flag_dom(i,j) = 1
        enddo
        do i = n_d,nx_max
            flag_dom(i,j) = 1
        enddo
        j = ny_max
        do i = 0,nx_max
            flag_dom(i,j) = 1
        enddo

C     (c) semiconductor-oxide interface
        j = 0
        do i = n_s+1,n_d-1
            flag_dom(i,j) = 2
        enddo

C     Define doping regions

       call domain_boundaries(1, 0, n_s, 0, n_j)
       call domain_boundaries(2, n_d, nx_max, 0, n_j)
       call domain_boundaries(3, n_s+1, n_d-1, 0,n_j)
       call domain_boundaries(4, 0, nx_max, n_j+1, ny_max)

C     Specify doping and initial potential in various regions

       call initialize_doping_potential()
       print*,'doping and potential initialized'
       print*,'  '

C     Initialize mesh

       call mesh_initialization()
       print*,'mesh initialized'
       print*,'  '

C     Calculate discretization coefficients for the Poisson equation

       call coefficients_poisson()
       print*,'poisson discretization coefficients calculated'
       print*,'  '


       return
       end


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      SPECIFY BOUNDARY FOR VARIOUS REGIONS
C      (Boundary regions are specified as integer numbers)
C
C      Important note: This is device independent routine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine domain_boundaries(n_reg,
     1             i_reg_min,i_reg_max,j_reg_min,j_reg_max)
       include 'scattering_table_parameters.h'

       common
     &/region_boundaries/ix_min(n_doping_regions),
     1                   ix_max(n_doping_regions),
     2                   jy_min(n_doping_regions),
     3                   jy_max(n_doping_regions)

       integer n_reg,i_reg_min,i_reg_max,j_reg_min,j_reg_max

       ix_min(n_reg)=i_reg_min
       ix_max(n_reg)=i_reg_max
       jy_min(n_reg)=j_reg_min
       jy_max(n_reg)=j_reg_max

       return
       end


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C     INITIALIZE MESH (assumes uniform mesh)
C
C     Important note: This is device independent routine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine mesh_initialization()
       include 'max_grid_size.h'

       common
     &/debye/debye_length
     &/mesh_size/nx_max,ny_max
     &/mesh/mesh_sizex,mesh_sizey
     &/grid/xx(-1:nx_size),yy(-1:ny_size)

       real mesh_sizex,mesh_sizey

       do i = -1,nx_max
            xx(i)=mesh_sizex/debye_length
       enddo
       do j = -1,ny_max
            yy(j)=mesh_sizey/debye_length
       enddo

       return
       end


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      INITIALIZE DOPING AND POTENTIAL IN VARIOUS REGIONS:
C
C        => Doping density is renormalized with intrinsic
C           carrier density
C        => Potential is renormalized with the thermal voltage
C
C      Important note: This is device independent routine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       subroutine initialize_doping_potential()

       include 'max_grid_size.h'
       include 'scattering_table_parameters.h'

       common
     &/intrinsic_density/intrinsic_carrier_density
     &/mesh_size/nx_max,ny_max
     &/potential/fai(0:nx_size,0:ny_size)
     &/doping/doping(0:nx_size,0:ny_size)
     &/coulomb/doping_density(n_doping_regions),
     1         Energy_debye(n_doping_regions)

       integer nx_max,ny_max,n
       real intrinsic_carrier_density
       real dop_term

       do i = 0,nx_max
       do j = 0,ny_max
          n = find_region(i,j)
          dop_term = doping_density(n)/
     1               intrinsic_carrier_density
          doping(i,j) = dop_term
          dop_term = 0.5*dop_term
          if(dop_term.gt.0)then
             factor = dop_term + sqrt(dop_term**2.+1.)
             fai(i,j)=log(dop_term)
          elseif(dop_term.le.0)then
             dop_term=-dop_term
             factor = dop_term + sqrt(dop_term**2.+1.)
             fai(i,j)=-log(dop_term)
          endif
11        continue
       enddo
       enddo

       return
       end


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C
C      Find a doping region based on node location
C
C      Important note: This is device independent routine
C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
       function find_region(i,j)
       include 'scattering_table_parameters.h'

       common
     &/doping_regions/n_region
     &/region_boundaries/ix_min(n_doping_regions),
     1                   ix_max(n_doping_regions),
     2                   jy_min(n_doping_regions),
     3                   jy_max(n_doping_regions)

       integer n_region,i,j,i_reg

       do n = 1,n_region
          if( i.ge.ix_min(n).and.i.le.ix_max(n).and.
     1        j.ge.jy_min(n).and.j.le.jy_max(n))then
              i_reg = n
              goto 11
          endif
       enddo
11     find_region = i_reg

       return
       end
